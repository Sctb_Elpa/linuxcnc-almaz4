#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ConfigParser
import StringIO
import argparse
import hashlib
import os.path
import subprocess
import sys

import cpuinfo

from Rockhopper.LinuxCNCWebSktSvr import main_loop as rh_main_loop
from Rockhopper_mod import my_rh_main

config_template_filename = 'almaz4.templae.ini'
result_config_filename = 'almaz4.work.ini'

hal_template_filename = 'almaz4.template.hal'
hal_config_filename = 'almaz4.work.hal'

hal_postgui_template_filename = 'almaz4.postgui.template.hal'

sim_buttons_panel = './sim_buttons_panel.py'


def merge_dicts(*dict_args):
    """
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    """
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result


def pin_unpack(pin):
    return pin / 100, pin % 100


def generate_pru_config(axis_n, pru_name='hpg'):
    """
    This code generates config for pru
    """

    return """
# ################
# Axis {axis_n}
# ################

# axis enable chain
newsig emcmot.0{axis_n}.enable bit
sets emcmot.0{axis_n}.enable FALSE

net emcmot.0{axis_n}.enable <= axis.{axis_n}.amp-enable-out
net emcmot.0{axis_n}.enable => hpg.stepgen.0{axis_n}.enable
#net emcmot.0{axis_n}.enable => hpg.pwmgen.0{axis_n}.out.0{axis_n}.enable

# position command and feedback
net emcmot.0{axis_n}.pos-cmd <= axis.{axis_n}.motor-pos-cmd
net emcmot.0{axis_n}.pos-cmd => hpg.stepgen.0{axis_n}.position-cmd

net motor.0{axis_n}.pos-fb <= hpg.stepgen.0{axis_n}.position-fb
net motor.0{axis_n}.pos-fb => axis.{axis_n}.motor-pos-fb

# timing parameters
setp {pru}.stepgen.0{axis_n}.dirsetup        [AXIS_{axis_n}]DIRSETUP
setp {pru}.stepgen.0{axis_n}.dirhold         [AXIS_{axis_n}]DIRHOLD

setp {pru}.stepgen.0{axis_n}.steplen         [AXIS_{axis_n}]STEPLEN
setp {pru}.stepgen.0{axis_n}.stepspace       [AXIS_{axis_n}]STEPSPACE

setp {pru}.stepgen.0{axis_n}.position-scale  [AXIS_{axis_n}]SCALE

setp {pru}.stepgen.0{axis_n}.maxvel          [AXIS_{axis_n}]STEPGEN_MAX_VEL
setp {pru}.stepgen.0{axis_n}.maxaccel        [AXIS_{axis_n}]STEPGEN_MAX_ACC

setp {pru}.stepgen.0{axis_n}.steppin         [AXIS_{axis_n}]STEP_PIN
setp {pru}.stepgen.0{axis_n}.dirpin          [AXIS_{axis_n}]DIR_PIN
""".format(axis_n=axis_n, pru=pru_name)


def get_buttons_map(config):
    buttons_map = {}
    for i in xrange(10):
        key = 'B{}'.format(i)
        try:
            v = int(config.get('PINS', key))
            buttons_map[key] = v
        except ConfigParser.NoOptionError:
            pass

    return buttons_map


def revers_interface_buttons(bmap):
    result = {}
    for i in xrange(10):
        key_orig = 'B{}'.format(i)
        key_new = 'B{}'.format(10 - i)
        try:
            result[key_new] = bmap[key_orig]
        except KeyError:
            pass
    return result


def get_home_map(config):
    homes = {}
    for axis in xrange(int(config.get('TRAJ', 'AXES'))):
        key = 'H{}'.format(axis)
        try:
            pin = int(config.get('PINS', key))
            is_inverted = config.get('PINS', 'H{}_INVERTED'.format(axis)) or False
            homes[key] = (pin, is_inverted)
        except ConfigParser.NoOptionError:
            pass

    return homes


class TextTemplate:
    def __init__(self, text):
        self.text = text

    def replace(self, placeholder, text):
        self.text = self.text.replace('#%%{}%%'.format(placeholder), text)
        return self.text

    def append(self, text):
        self.text += text

    def get(self):
        return self.text


class RawMultiConfigParser(ConfigParser.RawConfigParser):
    """
    Write lists as multy-key values
    Exsample:
        ini.set('SECTION', 'key', ['val1', 'val2'])

        [SECTION]
        key = val1
        key = val2
    """

    def write(self, fp):
        """Write an .ini-format representation of the configuration state."""
        if self._defaults:
            fp.write("[%s]\n" % ConfigParser.DEFAULTSECT)
            for (key, value) in self._defaults.items():
                fp.write("%s = %s\n" % (key, str(value).replace('\n', '\n\t')))
            fp.write("\n")
        for section in self._sections:
            fp.write("[%s]\n" % section)
            for (key, value) in self._sections[section].items():
                if key == "__name__":
                    continue
                if (value is not None) or (self._optcre == self.OPTCRE):
                    if isinstance(value, list):
                        res = []
                        for val in value:
                            res.append(" = ".join((key, str(val).replace('\n', '\n\t'))))
                        key = '\n'.join(res)
                    else:
                        key = " = ".join((key, str(value).replace('\n', '\n\t')))
                fp.write("%s\n" % (key))
            fp.write("\n")


def connect_bb_pin_digital_io(hal_data, pin_name, pin_number, digital_io, direction):
    p, n = pin_unpack(pin_number)
    pin_data = {'hader': p,
                'pin': n,
                'netname': "{}_net".format(pin_name),
                'ion': '{:02d}'.format(digital_io)}

    if direction == 'OUT':
        hal_data.replace('{}_net'.format(pin_name),
                         "net {netname} bb_gpio.p{hader}.out-{pin} <= motion.digital-out-{ion}".format(
                             **pin_data))
    else:
        hal_data.replace('{}_net'.format(pin_name),
                         "net {netname} bb_gpio.p{hader}.in-{pin} => motion.digital-in-{ion}".format(
                             **pin_data))


def generate_led_end_net(hal_data, pin_name, pin_number, digital_io):
    p, n = pin_unpack(pin_number)
    pin_data = {'hader': p,
                'pin': n,
                'netname': "{}_net".format(pin_name),
                'ion': '{:02d}'.format(digital_io)}
    hal_data.replace('{}_net'.format(pin_name), """
net start_enabled => green-led-or.in0
net do_and_{netname} green-led-or.in1 <= motion.digital-out-{ion}
net {netname} bb_gpio.p{hader}.out-{pin} <= green-led-or.out
""".format(**pin_data))


def generate_led_work_net(hal_data, pin_name, pin_number, digital_io):
    p, n = pin_unpack(pin_number)
    pin_data = {'hader': p,
                'pin': n,
                'netname': "{}_net".format(pin_name),
                'ion': '{:02d}'.format(digital_io)}
    hal_data.replace('{}_net'.format(pin_name), """
net {netname}_enable pwmgen.0.enable <= motion.digital-out-{ion}
net {netname} bb_gpio.p{hader}.out-{pin} <= pwmgen.0.pwm
setp pwmgen.0.pwm-freq 0.5
setp pwmgen.0.min-dc 0.4
setp pwmgen.0.min-dc 0.6
setp pwmgen.0.scale 100
setp pwmgen.0.value 50
""".format(**pin_data))


def generate_motor_output(hal_data, motor_pin_number):
    p, n = pin_unpack(motor_pin_number)
    pin_data = {'hader': p, 'pin': n}
    # M3 - on M5 - off
    hal_data.replace('motor_net',
                     "net motor_net bb_gpio.p{hader}.out-{pin} <= motion.spindle-on".format(**pin_data))


def generate_coolant_output(hal_data, coolant_pin_number):
    p, n = pin_unpack(coolant_pin_number)
    pin_data = {'hader': p,
                'pin': n}
    # M8 - on M9 - off
    hal_data.replace('pomp_net',
                     "net pomp_net bb_gpio.p{hader}.out-{pin} <= iocontrol.0.coolant-flood".format(**pin_data))


def generate_rotation_sensor(hal_data, yr_sensor_data):
    ROTATION_SENSOR_DI = '11'
    yr_sensor_nets = StringIO.StringIO()
    p, n = pin_unpack(yr_sensor_data[0])
    yr_sensor_nets.write('net yr_net bb_gpio.p{}.in-{} => motion.digital-in-{}\n'.format(p, n, ROTATION_SENSOR_DI))
    yr_sensor_nets.write('setp bb_gpio.p{}.in-{}.invert {}\n'.format(p, n, yr_sensor_data[1]))
    hal_data.replace('yr_net', yr_sensor_nets.getvalue())


def generate_rotation_enabler(hal_data, yr_pin, ion, yr_invert):
    yr_enabler_net = StringIO.StringIO()
    p, n = pin_unpack(yr_pin)
    pin_data = {'hader': p, 'pin': n, 'ion': '{:02d}'.format(ion), 'invert': yr_invert}
    yr_enabler_net.write('setp bb_gpio.p{hader}.out-{pin}.invert {invert}\n'.format(**pin_data))
    yr_enabler_net.write("""net yr_ex yr_out_enable.in0 <= motion.digital-out-{ion}
net estop-loopout yr_out_enable.in1
net yr_net_out bb_gpio.p{hader}.out-{pin} <= yr_out_enable.out\n""".format(**pin_data))
    # yr_enabler_net.write('net yr_net_out bb_gpio.p{hader}.out-{pin} <= motion.digital-out-{ion}\n'.format(**pin_data))

    hal_data.replace('rotation_enabler', yr_enabler_net.getvalue())


def generate_buttons_nets(hal_data, buttons_map):
    button_nets = StringIO.StringIO()
    for button_net, pin in buttons_map.iteritems():
        is_invert = 'TRUE'
        hader, pin_n = pin_unpack(pin)
        source_pin = "bb_gpio.p{}.in-{}".format(hader, pin_n)
        dest_pin_n = button_net[1:]
        if len(dest_pin_n) == 1:
            dest_pin_n = '0' + dest_pin_n
        dest_pin = 'motion.digital-in-{}'.format(dest_pin_n)
        button_nets.write('setp {}.invert {}\n'.format(source_pin, is_invert))
        button_nets.write('net {} {} => {}\n'.format(button_net, source_pin, dest_pin))
    hal_data.replace('button_nets', button_nets.getvalue())


def generate_home_sensors_nets(hal_data, homemap):
    home_nets = StringIO.StringIO()
    for home_net, (h, is_invert) in homemap.iteritems():
        hader, pin_n = pin_unpack(h)
        source_pin = "bb_gpio.p{}.in-{}".format(hader, pin_n)
        dest_pin = 'axis.{}.home-sw-in'.format(home_net[1:])
        home_nets.write('setp {}.invert {}\n'.format(source_pin, is_invert))
        home_nets.write('net {} {} => {}\n'.format(home_net, source_pin, dest_pin))
        # --
        din_debug_pin_pin = 'motion.digital-in-{}'.format(int(home_net[1:]) + 10)
        home_nets.write('net {} => {}\n'.format(home_net, din_debug_pin_pin))
        # --
    hal_data.replace('home_switches', home_nets.getvalue())


def get_input_config(config, key):
    return (int(config.get('PINS', key)),
            config.get('PINS', 'YR_INVERTED') or False)


def generate_main_control_buttons(hal_data, main_control_buttons_map, enable_data):
    is_invert = 'TRUE'
    main_control_buttons = StringIO.StringIO()

    # start -> Halui.programm.run + Halui.programm.auto <= button & Halui.program.is-idle & motion.digital-out-05
    main_control_buttons.write("""
net idle_net start_enable.in0 <= halui.program.is-idle
net start_enable_net start_enable.in1 <= motion.digital-out-05
net start_enabled start_condition.in0 <= start_enable.out
net start_net halui.mode.auto <= start_condition.out
net start_net => halui.program.run
""")

    hader, pin_n = pin_unpack(main_control_buttons_map['START'][0])
    source_pin = "bb_gpio.p{}.in-{}".format(hader, pin_n)
    main_control_buttons.write('setp {}.invert {}\n'.format(source_pin, is_invert))
    main_control_buttons.write('net start_btn_net {} => start_condition.in1\n'.format(source_pin))

    # stop
    main_control_buttons.write("""
net estop-loopout iocontrol.0.emc-enable-in <= estop0.ok-out
net estop-loopin iocontrol.0.user-enable-out => estop0.ok-in
net estop-reset iocontrol.0.user-request-enable => estop0.reset

# enable hack ENABLE-PIN = estop0.ok-out & !motion.digital-out-06
net estop-loopout => enable_out_hack.in0
net estop-hack motion.digital-out-06 => enable_out_hack.in1
net enable-output enable_out_hack.out => bb_gpio.p{enable_hader}.out-{enable_pin}
setp bb_gpio.p{enable_hader}.out-{enable_pin}.invert [PINS]ENABLE_INVERTED
""".format(**enable_data))

    hader, pin_n = pin_unpack(main_control_buttons_map['STOP'][0])
    source_pin = "bb_gpio.p{}.in-{}".format(hader, pin_n)
    main_control_buttons.write('setp {}.invert {}\n'.format(source_pin, is_invert))
    main_control_buttons.write('net remote-estop estop0.fault-in <= {}\n'.format(source_pin))

    hal_data.replace('main_ctrl_buttons', main_control_buttons.getvalue())


def generate_hal(is_sim, config):
    # hal
    with open(hal_template_filename, 'r') as hal_template:
        hal_data = TextTemplate(hal_template.read())

    # modify template
    if is_sim:
        hal_data.append("""
# virtual buttons panel
loadusr -Wn sim_buttons {}

net b7 sim_buttons.b7 => motion.digital-in-07
net b9 sim_buttons.b9 => motion.digital-in-09
net b4 sim_buttons.b4 => motion.digital-in-04
net b6 sim_buttons.b6 => motion.digital-in-06
net b1 sim_buttons.b1 => motion.digital-in-01
net b3 sim_buttons.b3 => motion.digital-in-03
        """.format(sim_buttons_panel))
    else:
        buttons_map = get_buttons_map(config)

        if config.get('PINS', 'IS_DISPLAY_INVERTED') == 'TRUE':
            buttons_map = revers_interface_buttons(buttons_map)

        homemap = get_home_map(config)

        axes_count = int(config.get('TRAJ', 'AXES'))
        enable_pin = int(config.get('PINS', 'ENABLE_PIN'))

        motor_pin = int(config.get('PINS', 'MOTOR_PIN'))
        pomp_pin = int(config.get('PINS', 'POMP_PIN'))
        rotation_enabler_pin = int(config.get('PINS', 'Y_ROTATION_ENABLER'))
        rotation_enabler_pin_invert = config.get('PINS', 'Y_ROTATION_INVERTED')

        led_end_pin = int(config.get('PINS', 'LED_END_PIN'))
        led_work_pin = int(config.get('PINS', 'LED_WORK_PIN'))

        main_control_buttons = {
            'START': get_input_config(config, 'BUTTON_START'),
            'STOP': get_input_config(config, 'BUTTON_STOP')
        }
        rotation_sensor = get_input_config(config, 'YR')

        pru_name = config.get('PRUCONF', 'PRU_NICKNAME')

        _home_pins = [pin for key, (pin, _) in homemap.iteritems()]
        _main_control_buttons = [pin for key, (pin, _) in main_control_buttons.iteritems()]

        def semicolon_separeted_numbers(l):
            sl = map(str, l)
            return ','.join(sl)

        hal_data.replace('setup', 'loadusr -w ./setup.sh')

        hal_data.replace('loadrt', """
loadrt trivkins
loadrt tp
loadrt pwmgen output_type=0
loadrt and2 names=start_condition,start_enable,enable_out_hack,red-led-and,yr_out_enable
loadrt or2 names=green-led-or
loadrt estop_latch names=estop0
loadrt threads name1=slowt fp1=1 period1=1000000
loadrt [EMCMOT]EMCMOT servo_period_nsec=[EMCMOT]SERVO_PERIOD num_joints=[TRAJ]AXES tp=tp kins=trivkins num_dio=[EMCMOT]DIGITAL_IO
loadrt [PRUCONF](DRIVER) prucode=$(HAL_RTMOD_DIR)/[PRUCONF](PRUBIN) [PRUCONF](CONFIG) halname=[PRUCONF]PRU_NICKNAME""")

        hal_data.replace('bbb_pins', """
loadrt hal_bb_gpio output_pins={outputs} input_pins={inputs}
""".format(outputs=semicolon_separeted_numbers(
            (enable_pin, motor_pin, pomp_pin, rotation_enabler_pin, led_end_pin, led_work_pin)),
            inputs=semicolon_separeted_numbers(buttons_map.values() + _home_pins + _main_control_buttons +
                                               [rotation_sensor[0]])))

        hal_data.replace('threads', """
addf {pru_name}.capture-position          servo-thread
addf motion-command-handler               servo-thread
addf motion-controller                    servo-thread
addf {pru_name}.update                    servo-thread
addf bb_gpio.read                         servo-thread
addf bb_gpio.write                        servo-thread

addf start_condition                      slowt
addf start_enable                         slowt
addf enable_out_hack                      slowt
addf yr_out_enable                        slowt
addf green-led-or                         slowt
addf red-led-and                          slowt
addf estop0                               slowt
addf pwmgen.update                        slowt
addf pwmgen.make-pulses                   slowt
""".format(pru_name=pru_name))

        p, n = pin_unpack(enable_pin)
        enable_data = {'enable_hader': p, 'enable_pin': n}
        hal_data.replace('Enable', """
net tool-prep-loop iocontrol.0.tool-prepare => iocontrol.0.tool-prepared
net tool-change-loop iocontrol.0.tool-change => iocontrol.0.tool-changed""")

        generate_motor_output(hal_data, motor_pin)
        generate_coolant_output(hal_data, pomp_pin)

        generate_led_end_net(hal_data, 'led_end', led_end_pin, 3)
        generate_led_work_net(hal_data, 'led_work', led_work_pin, 4)
        connect_bb_pin_digital_io(hal_data, 'led_work', led_work_pin, 4, 'OUT')

        generate_rotation_sensor(hal_data, rotation_sensor)
        generate_rotation_enabler(hal_data, rotation_enabler_pin, 2, rotation_enabler_pin_invert)
        generate_buttons_nets(hal_data, buttons_map)
        generate_home_sensors_nets(hal_data, homemap)
        generate_main_control_buttons(hal_data, main_control_buttons, enable_data)

        # generate pru step generators
        for axis_n in range(axes_count):
            hal_data.append(generate_pru_config(axis_n, pru_name))

    is_hal_correct = False
    if os.path.isfile(hal_config_filename):
        with open(hal_config_filename, 'r') as existing_hal_config:
            m_e = hashlib.md5()
            m_e.update(existing_hal_config.read())
            m_n = hashlib.md5()
            m_n.update(hal_data.get())
            is_hal_correct = m_e.digest() == m_n.digest()
    if not is_hal_correct:
        with open(hal_config_filename, 'wb') as hal_config:
            hal_config.write(hal_data.get())


def generate_ini(is_sim):
    # ini
    config_template = RawMultiConfigParser()
    config_template.optionxform = str  # case sensitive
    config_template.read([config_template_filename])

    if is_sim:
        # sim
        config_template.set('DISPLAY', 'DISPLAY', 'axis')
        config_template.set('EMC', 'MACHINE', 'almaz4-sim')
        config_template.set('HAL', 'HALFILE', ['core_sim.hal', hal_config_filename])
    else:
        # work
        config_template.set('DISPLAY', 'DISPLAY', 'linuxcncrsh')
        #config_template.set('DISPLAY', 'DISPLAY', 'mini')
        config_template.set('EMC', 'MACHINE', 'almaz4')
        config_template.set('HAL', 'HALFILE', hal_config_filename)
    is_ini_correct = False
    new_config = StringIO.StringIO()
    config_template.write(new_config)
    if os.path.isfile(result_config_filename):
        with open(result_config_filename, 'r') as existing_config_file:
            m_e = hashlib.md5()
            m_e.update(existing_config_file.read())
            m_n = hashlib.md5()
            m_n.update(new_config.getvalue())
            is_ini_correct = m_e.digest() == m_n.digest()
    if not is_ini_correct:
        with open(result_config_filename, 'wb') as existing_config_file:
            existing_config_file.write(new_config.getvalue())

    return config_template


def cd():
    import os

    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)


def current_path():
    return os.path.dirname(os.path.realpath(__file__))


def main():
    """
    Оопределить по uname на чем запуск
    1. Если не arm, то запускаем симуляцию. Физические кнопки заменет коно с кнопками, еще запущен halmeter
    2. Если arm, то запускаем полноцелнный режим. Физические кнопки подключены
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--sim', default=False, action='store_true')
    parser.add_argument('--remotegui', default=False, action='store_true')

    args = parser.parse_args()

    is_sim = args.sim or ('arm' not in cpuinfo.get_cpu_info()['raw_arch_string'])

    cd()

    config = generate_ini(is_sim)
    generate_hal(is_sim, config)

    def start_gui():
        newpyhonpath = os.pathsep.join([os.getenv("PYTHONPATH", ""),
                                        os.path.join(current_path(), 'GtkClient')])
        print("PYTHONPATH={}".format(newpyhonpath))
        os.putenv("PYTHONPATH", newpyhonpath)
        subprocess.Popen([sys.executable, 'GtkClient/start_gui.py'])

    # start GUI if local
    if not args.remotegui:
        rh_main_loop.add_callback(start_gui)

    # for Rockhopper server
    my_rh_main(result_config_filename)


if __name__ == "__main__":
    main()
    print('Server stopped.')
    exit(0)
