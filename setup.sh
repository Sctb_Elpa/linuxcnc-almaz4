#!/bin/bash

dtbo_err () {
	echo "Error loading device tree overlay file: $DTBO" >&2
	exit 1
}

pin_err () {
	echo "Error exporting pin:$PIN" >&2
	exit 1
}

dir_err () {
	echo "Error setting direction:$DIR on pin:$PIN" >&2
	exit 1
}

state_err() {
    echo "Error setting value:$STATE on pin:$PIN" >&2
    exit 1
}

SLOTS=/sys/devices/bone_capemgr.*/slots

# Make sure required device tree overlay(s) are loaded
for DTBO in BB-ALMAZ4 ; do

	if grep -q $DTBO $SLOTS ; then
		echo $DTBO overlay found
	else
		echo "Loading $DTBO overlay"
		sudo -A su -c "echo $DTBO > $SLOTS" || dtbo_err
		sleep 1
	fi
done;

if [ ! -r /sys/class/uio/uio0 ] ; then
	echo PRU control files not found in /sys/class/uio/uio0 >&2
	exit 1;
fi

# Export GPIO pins
# This really only needs to be done to enable the low-level clocks for the GPIO
# modules.  There is probably a better way to do this...
while read PIN DIR STATE JUNK ; do
        case "$PIN" in
        ""|\#*)	
		continue ;;
        *)
		[ -r /sys/class/gpio/gpio$PIN ] && continue
		        echo exporting pin $PIN $DIR $STATE $JUNK
                sudo -A su -c "echo $PIN > /sys/class/gpio/export" || pin_err
		        sudo -A su -c "echo $DIR > /sys/class/gpio/gpio$PIN/direction" || dir_err
		        if [[ "$DIR" == "out" ]]; then
		            sudo -A su -c "echo $STATE > /sys/class/gpio/gpio$PIN/value" || dir_err
		        fi
        ;;
        esac

done <<- EOF
    #buttons
	7	in	I  # B7
	65  in  I  # B9
	27  in  I  # B4
	22  in  I  # B5
	46  in  I  # B1
	61  in  I  # B3

    # X
    44  out 0  # STEP2
    49  out 0  # DIR2

    # Y
    15 out  0  # DIR1 (Y_ROTATION_ENABLER)

	# INPUTS
	51  in  I  # H0
	48  in  I  # YR
	60  in  I  # START
	30  in  I  # STOP

	# OUTPUTS
	45 out  0  # ENABLE
	2  out  0  # MOTOR
	3  out  0  # PUMP
	26 out  0  # LED_END
	68 out  0  # LED_WORK

	# RESERVED
	69 out  0  # STEP1
	31 in   I  # IN3

EOF
