#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk
import os
import sys

import gladevcp.makepins
import hal

xmlname = os.path.join(os.path.dirname(sys.argv[0]), "sim_buttons.glade")


def main():
    builder = gtk.Builder()
    builder.add_from_file(xmlname)
    halcomp = hal.component("sim_buttons")
    window = builder.get_object("window1")
    window.show()
    window.connect('destroy', on_window_destroy)
    gladevcp.makepins.GladePanel(halcomp, xmlname, builder, None)
    halcomp.ready()
    try:
        gtk.main()
    except KeyboardInterrupt:
        print('Sim buttons panel closed')


def on_window_destroy(widget, data=None):
    gtk.main_quit()


if __name__ == "__main__":
    main()
