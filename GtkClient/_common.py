# -*- coding: utf-8 -*-

import math

import numpy as np


class Point:
    def __init__(self, x=0.0, y=0.0):
        self.x = x
        self.y = y

    def copy(self):
        return Point(self.x, self.y)

    def __str__(self):
        return "Point({}, {})".format(self.x, self.y)

    def to_matrix_2d(self):
        return np.array([self.x, self.y, 1])


class Rect:
    def __init__(self, p1=Point(), p2=Point()):
        self.p1 = p1
        self.p2 = p2

    def oposit_point(self, point):
        simmetrical_center = self.center()
        teta = math.pi
        central_symetric_matrix = np.matrix([
            [math.cos(teta), math.sin(teta), 0],
            [-math.sin(teta), math.cos(teta), 0],
            [-simmetrical_center.x * (math.cos(teta) - 1) + simmetrical_center.y * math.sin(teta),
             -simmetrical_center.x * math.sin(teta) - simmetrical_center.y * (math.cos(teta) - 1), 1]])
        res = point.to_matrix_2d().dot(central_symetric_matrix)
        return Point(res.item(0), res.item(1))

    def center(self):
        return Point((self.p1.x + self.p2.x) / 2.0, (self.p1.y + self.p2.y) / 2.0)
