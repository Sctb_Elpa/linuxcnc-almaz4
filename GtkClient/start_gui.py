#!/usr/bin/env python

import argparse

from controller import Controller
from gui import Gui
from remote import MyWSClientFactory
from settings import Settings
from GtkClient.GuiStates.GuiStates import button7, button9, button4, button6, button1, button3


def build_dio_map():
    return {
        # Inputs
        button7: 7,
        button9: 9,
        button4: 4,
        button6: 6,
        button1: 1,
        button3: 3,

        'H0': 10,
        'YR': 11,

        # Outputs
        'yr_trigger': 2,
        'led_green': 3,
        'led_red': 4,
        'start_enable': 5,
        'enable_ctl': 6
    }


def main():
    from twisted.internet import gtk2reactor
    gtk2reactor.install()
    from twisted.internet import reactor

    parser = argparse.ArgumentParser()
    parser.add_argument('--remote', type=str, default=None)

    args = parser.parse_args()
    if not args.remote:
        print('Starting local GIU')
        adr = 'localhost'
        port = 8000
    else:
        print('Starting remote GUI for {}'.format(args.remote))
        if ':' not in args.remote:
            port = 8000
            adr = args.remote
        else:
            adr, port = args.remote.split(':')
            port = int(port)

    factory = MyWSClientFactory(adr, port, protocols=['linuxcnc'])
    # factory.debug = True

    settings = Settings()
    dio_map = build_dio_map()

    _gui = Gui(settings, dio_map, factory.reactor)
    controller = Controller(factory, settings, dio_map)

    factory.emitter.connect('logged_in', _gui.connected)

    _gui.connect('reconnect', controller.reconnect)
    _gui.connect('process_command_req', controller.process_command)
    factory.emitter.connect('watch_got', _gui.on_watch_variable_changed)

    _gui.reset()
    _gui.show()
    _gui.on_window_closed(window_closed)
    reactor.run()

    settings.save_settings()


def window_closed(sender):
    from twisted.internet import reactor
    reactor.stop()


if __name__ == "__main__":
    main()
