# -*- coding: utf-8 -*-

from IGuiState import IGuiState
from GtkClient.Point import Point
import time


class RunningControlState(IGuiState):
    def __init__(self):
        self.widgets = []

        w = IGuiState.create_gtk_label()
        w.set_markup(u"<span size='xx-large' foreground=\"#e53935\">Установка работает.</span>")
        self.widgets.append((w, Point(225, 100)))

        w = IGuiState.create_gtk_label()
        w.set_markup(u"""<span size='large'>Чтобы прервать работу нажмите <b><span foreground="#e53935">КРАСНУЮ</span>\
</b> кнопку на панели</span>""")
        self.widgets.append((w, Point(75, 160)))

        self.mode_label = IGuiState.create_gtk_label()
        self.mode_label.set_markup(u"<span size='xx-large'>Режим: ----</span>")
        self.widgets.append((self.mode_label, Point(75, 210)))

        self.W = IGuiState.create_gtk_label()
        self.W.set_markup(u"<span size='xx-large'>Рабочий ход: --- / --- мм</span>")
        self.widgets.append((self.W, Point(75, 250)))

        self.Wdest = 0

        self.Pass = IGuiState.create_gtk_label()
        self.Pass.set_markup(u"<span size='xx-large'>Проход №: --- / ---</span>")
        self.widgets.append((self.Pass, Point(75, 280)))

        self.YR_N = 0
        self.Pases = 0
        self.YR_pp = 0

        self.last_yr_trigger = 0
        self.filter_time = 0.5

    def apply(self, gui):
        gui.reset_buttons()
        gui.clear_central_field()
        gui.add_central_field(self.widgets)

        RunningControlState.register_finish_cb(gui)

        work_mode = gui.get_settings_value('Work Mode')
        mode_name = IGuiState.work_modes_table[work_mode]
        self.mode_label.set_markup(
            u"<span size='xx-large'>Режим работы:</span> {}".format(mode_name))

        self.Wdest = gui.get_settings_value('Cutting depth')

        self.YR_N = 0
        self.YR_pp = gui.get_settings_value('YR turns pre plate')
        if work_mode == 2:
            self.Pases = gui.get_settings_value('Plate count')
            self.Pass.set_markup(u"<span size='xx-large'>Проход №: <b>1</b> / {Pases}</span>".format(
                Pases=self.Pases))
            self.Pass.set_visible(True)
        else:
            self.Pases = 1
            self.Pass.set_visible(False)

        gui.subscribe_position(self.update_position)
        gui.subscribe_yr_state(self.pass_counter)

    def release(self, gui):
        gui.unsubscribe_yr_state(self.pass_counter)
        gui.unsubscribe_position(self.update_position)

    @staticmethod
    def register_finish_cb(gui):
        def finished(_):
            gui.on_interp_state_got('EMC_TASK_INTERP_IDLE', None, None)
            gui.switch_state('HOME_SCREEN')

        gui.on_interp_state_got('EMC_TASK_INTERP_IDLE', finished, None)

    def update_screen(self, gui):
        pass

    def update_position(self, current_pos):
        self.W.set_markup(u"<span size='xx-large'>Рабочий ход: <b>{X:.2f}</b> / {W:.2f} мм</span>".format(
            X=current_pos[0], W=self.Wdest))

    def increment_YR_counter(self):
        self.YR_N += 1

        p = self.YR_N / self.YR_pp + 1
        self.Pass.set_markup(u"<span size='xx-large'>Проход №: <b>{p}</b> / {Pases}</span>".format(
            p=p, Pases=self.Pases))

    def pass_counter(self, new_yr_state):
        if not new_yr_state:
            self.last_yr_trigger = time.time()
        elif self.last_yr_trigger != 0:
            diff = time.time() - self.last_yr_trigger
            if diff > self.filter_time:
                self.increment_YR_counter()
