# -*- coding: utf-8 -*-

from IGuiState import IGuiState
from GtkClient.Point import Point
from GuiStates import button7, button9, gen_value_markup
from EditorButtons import EditorButtons


class SingleValueEditor(IGuiState, EditorButtons):
    def __init__(self, value_name, step, parent_state, editor_prompt, units='', hook=None):
        self.hook = hook
        self.value_name = value_name
        self.step = step
        self.parent_state = parent_state
        self.gui = None
        self.units = units

        self.widgets = []

        self.editor_prompt = IGuiState.create_gtk_label()
        self.editor_prompt.set_markup(editor_prompt)
        self.value_display = IGuiState.create_gtk_label()

        self.markup = gen_value_markup(step)

        self.update_screen(None)
        self.widgets.append((self.editor_prompt, Point(-0.5, 150)))
        self.widgets.append((self.value_display, Point(-0.5, -0.5)))

    def _apply_common(self, gui):
        self.gui = gui

        gui.set_button_text(button7, u'Отмена')
        gui.set_button_enabled(button7, True)
        gui.set_button_callback(button7, self.cancel_edit, gui.get_settings_value(self.value_name))

        gui.set_button_text(button9, u'ОК')
        gui.set_button_enabled(button9, True)
        gui.set_button_callback(button9, self.close_editor, None)

        gui.clear_central_field()
        gui.add_central_field(self.widgets)

        gui.install_settings_change_hook(self.hook)

    def apply(self, gui):
        EditorButtons.apply(self, gui)
        self._apply_common(gui)
        self.update_screen(gui)

    def cancel_edit(self, value):
        self.gui.set_settings_value(self.value_name, value)
        self.close_editor()

    def close_editor(self, dummy=0):
        self.gui.switch_state(self.parent_state)

    def change_value(self, step):
        self.gui.change_settings((self.value_name, step))

    def update_screen(self, gui):
        v = gui.get_settings_value(self.value_name) if gui else 100

        self.value_display.set_markup(self.markup.format(
            v, self.units))

    def release(self, gui):
        gui.install_settings_change_hook(None)
