# -*- coding: utf-8 -*-

import gtk
from IGuiState import IGuiState
from GtkClient.Point import Point
from GuiStates import almaz_lube_400x270, almaz_no_lube_400x270


class CentralDescription:
    @staticmethod
    def simple_formatter(x):
        return '<span>{}</span>'.format(x)

    @staticmethod
    def bool_formatter(x):
        if x == 0:
            return u'<span foreground="#c62828">Нет</span>'
        else:
            return u'<span foreground="#2e7d32">Да</span>'

    params_list_template = u"<span size='medium' foreground='#2962ff'>{}</span>"

    params_list = (
        params_list_template.format(u'Подача, мм/мин:'),
        params_list_template.format(u'Глубина реза, мм:'),
        params_list_template.format(u'Оборотов оси Y на одну пластину:'),
        params_list_template.format(u'Пластин в блоке:'),
        params_list_template.format(u'СОЖ включен:'),
        params_list_template.format(u'Шпиндель включен:'),
    )

    params2key_table = {
        params_list[0]: ('G1 speed', simple_formatter.__func__),
        params_list[1]: ('Cutting depth', lambda x: '{:.1f}'.format(x)),
        params_list[2]: ('YR turns pre plate', simple_formatter.__func__),
        params_list[3]: ('Plate count', simple_formatter.__func__),
        params_list[4]: ('Lube enabled', bool_formatter.__func__),
        params_list[5]: ('Spindel Enabled', bool_formatter.__func__),
    }

    def __init__(self):
        self.widgets = []

        self.image_lube = gtk.image_new_from_pixbuf(almaz_lube_400x270)
        self.image_no_lube = gtk.image_new_from_pixbuf(almaz_no_lube_400x270)
        self.widgets.append((self.image_lube, Point(-0.28, -0.6)))
        self.widgets.append((self.image_no_lube, Point(-0.28, -0.6)))

        row_count = len(CentralDescription.params_list)
        self._params_table = gtk.Table(row_count, 2)
        self._params_table.show()
        self._params_table.set_col_spacing(0, 20)
        for i in xrange(row_count):
            self._params_table.set_row_spacing(i, 15)
        self.widgets.append((self._params_table, Point(-0.705, -0.56)))

        self._params_table_values = {}
        row = 0
        for name in CentralDescription.params_list:
            description = IGuiState.create_gtk_label()
            description.set_markup(name)
            description.set_alignment(1, .5)
            self._params_table.attach(description, 0, 1, row, row + 1)
            value = IGuiState.create_gtk_label()
            value.set_alignment(0, .5)
            self._params_table.attach(value, 1, 2, row, row + 1)
            row += 1
            self._params_table_values[value] = CentralDescription.params2key_table[name]

    def attach(self, gui):
        gui.add_central_field(self.widgets)

    def update(self, gui):
        self.update_pic(gui.get_settings_value('Lube enabled') != 0)
        self.fill_params_table_values(gui)

    def update_pic(self, lube_enabled):
        if lube_enabled:
            self.image_lube.show()
            self.image_no_lube.hide()
        else:
            self.image_no_lube.show()
            self.image_lube.hide()

    def fill_params_table_values(self, gui):
        for lbl in self._params_table_values:
            key, formatter = self._params_table_values[lbl]
            lbl.set_markup(formatter(gui.get_settings_value(key)))
