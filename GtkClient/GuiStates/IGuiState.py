# -*- coding: utf-8 -*-

from abc import ABCMeta, abstractmethod
from gladevcp.led import HAL_LED
import gtk


class IGuiState:
    """ Интерфейс состояния окна """

    __metaclass__ = ABCMeta

    _work_modes_template = u"<span size='x-large' foreground='#{}'>{}</span>"

    work_modes_table = (
        _work_modes_template.format('0091ea', u'Ручной'),
        _work_modes_template.format('aa00ff', u'Одиночный'),
        _work_modes_template.format('00b0ff', u'Авто'),
    )

    @abstractmethod
    def apply(self, gui):
        """ Применить состояние к окну """

    def release(self, gui):
        """ Отменить изменения """
        gui.commit_changes()

    @abstractmethod
    def update_screen(self, gui):
        """ Обновить экран """

    @staticmethod
    def common_apply(data, gui):
        for btn in data:
            info = data[btn]
            gui.set_button_text(btn, info['text'])
            if 'is_tocl' in info:
                gui.set_button_toggle_on_click(btn, info['is_tocl'])
            else:
                gui.set_button_toggle_on_click(btn, False)
            gui.set_button_enabled(btn, True)
            callback_info = info['callback_info']
            if callback_info:
                btn_callback, btn_cookie = callback_info
                gui.set_button_callback(btn, gui[btn_callback], btn_cookie)
            elif 'local_callback' in info:
                local_btn_callback, btn_cookie = info['local_callback']
                gui.set_button_callback(btn, local_btn_callback, btn_cookie)

    @staticmethod
    def create_gtk_label():
        label = gtk.Label()
        label.set_justify(gtk.JUSTIFY_CENTER)
        label.show()
        return label

    @staticmethod
    def create_hal_led():
        led = HAL_LED()
        led.has_hal_pin = False
        led.led_blink_rate = 1
        led.show()
        return led
