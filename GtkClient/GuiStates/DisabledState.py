# -*- coding: utf-8 -*-

from IGuiState import IGuiState
from GtkClient.Point import Point
from GuiStates import button7, button9


class DisabledState(IGuiState):
    def __init__(self):
        self.widgets = []

        w = IGuiState.create_gtk_label()
        w.set_markup(u"""<span size='xx-large' foreground='#0091ea'>Установка резки кварца Алмаз 4</span>
<span size='large'>Для включения установки нажмите кнопку "Включить"</span>""")
        self.widgets.append((w, Point(150, 100)))

    def apply(self, gui):
        gui.reset_buttons()

        gui.set_button_text(button7, u'Выключить установку')
        gui.set_button_enabled(button7, True)
        gui.set_button_callback(button7, DisabledState._machine_poweroff, gui)

        gui.set_button_text(button9, u'Включить')
        gui.set_button_enabled(button9, True)
        gui.set_button_callback(button9, DisabledState._machine_prepare, gui)

        gui.clear_central_field()
        gui.add_central_field(self.widgets)

        gui.reconnect()
        gui.hold_interface(None)

    @staticmethod
    def _machine_poweroff(gui):
        gui.machine_start_program("SystemShutdown", None)

    @staticmethod
    def _machine_prepare(gui):
        def on_prepared(_=None):
            gui.switch_state("HOME_SCREEN")

        gui.machine_start_program("Prepare", on_prepared)
        # on_prepared()

    def update_screen(self, gui):
        pass
