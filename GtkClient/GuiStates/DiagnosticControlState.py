# -*- coding: utf-8 -*-

from IGuiState import IGuiState
from GtkClient.Point import Point
from GuiStates import button7, button9, button4, button6, button1, button3
from GtkClient.FollowHooks import FollowXYHook


class DiagnosticControlState(IGuiState):
    state_defs = {
        button1: {'text': u'Светофор', 'callback_info': ('switch_state', "TEST_LIGHTS")},
        button3: {'text': u'Выход', 'callback_info': ('switch_state', "HOME_SCREEN")}
    }

    def __init__(self):
        self.widgets = []

        w = IGuiState.create_gtk_label()
        w.set_markup(u"""<span size='xx-large' foreground='#0091ea'>Диагностика</span>
<span size='large'>Включить или выключить компаненты установки в ручном режиме</span>""")
        self.widgets.append((w, Point(100, 100)))

        self.spindel_state = False
        self.lube_state = False

    def apply(self, gui):
        self.spindel_state = False
        self.lube_state = False

        gui.reset_buttons()
        IGuiState.common_apply(DiagnosticControlState.state_defs, gui)

        gui.set_button_text(button7, u'Ось X')
        gui.set_button_enabled(button7, True)
        gui.set_button_callback(button7, gui.diag_interface,
                                ('X', 1, 'DIAGNOSTIC', u"<span size='x-large'>Ручное перемещение оси X</span>",
                                 FollowXYHook(gui), u'мм'))

        gui.set_button_text(button9, u'Ось Y: Шагнуть')
        gui.set_button_enabled(button9, True)
        gui.set_button_callback(button9, self._trigger_dy, gui)

        gui.set_button_text(button4, u'Шпиндель')
        gui.set_button_enabled(button4, True)
        gui.set_button_callback(button4, self._spindel_toggle, gui)
        gui.set_button_toggle_on_click(button4, True)

        gui.set_button_text(button6, u'СОЖ')
        gui.set_button_enabled(button6, True)
        gui.set_button_callback(button6, self._lube_toggle, gui)
        gui.set_button_toggle_on_click(button6, True)

        gui.clear_central_field()
        gui.add_central_field(self.widgets)

    def update_screen(self, gui):
        pass

    def release(self, gui):
        gui.MDI_nonblock('M5 M9')

    def _trigger_dy(self, gui):
        gui.trigger_dy_nonblock()

    def _spindel_toggle(self, gui):
        self.spindel_state = not self.spindel_state
        gui.MDI_nonblock('M3 S1' if self.spindel_state else 'M5')

    def _lube_toggle(self, gui):
        self.lube_state = not self.lube_state
        gui.MDI_nonblock('M8' if self.lube_state else 'M9')

