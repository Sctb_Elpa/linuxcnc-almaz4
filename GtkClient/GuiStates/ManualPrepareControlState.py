# -*- coding: utf-8 -*-

from twisted.internet import task

from IGuiState import IGuiState
from GtkClient.Point import Point
from GuiStates import button7, button9


class ManualPrepareControlState(IGuiState):
    """
    При открытии окна (программа загружена!)
    3. Освободить оси
    4. Включить возможность нажать СТАРТ
    При отмене
    1. Выключить возможность нажать СТАРТ
    2. Выключить СОЖ
    3. Захватить оси
    4. Переход в главное окно
    При нажатии "Старт"
    1. Установить внешний энеёбл
    2. Переход в состояние WORKING
    """
    def __init__(self):
        self.widgets = []
        self.lube_state = False

        w = IGuiState.create_gtk_label()
        w.set_markup(u"""<span size='xx-large' foreground='#0091ea'>Подводка</span>
<span size='large'>Подведите заготовку к инструменту вручную как показано на рисунке</span>
<span size='large'>После этого нажмите кнопу <b>СТАРТ</b> для запуска обработки</span>""")
        self.widgets.append((w, Point(50, 100)))
        self.enable_disabler_do_pin = None

    def apply(self, gui):
        self.enable_disabler_do_pin = gui.get_dio_by_name('enable_ctl')
        self.lube_state = False

        gui.reset_buttons()

        gui.set_button_text(button7, u'СОЖ')
        gui.set_button_enabled(button7, True)
        gui.set_button_callback(button7, self._lube_toggle, gui)

        gui.set_button_text(button9, u'Отмена')
        gui.set_button_enabled(button9, True)
        gui.set_button_callback(button9, self._cancel, gui)

        gui.clear_central_field()
        gui.add_central_field(self.widgets)

        ManualPrepareControlState._register_run_watcher(gui)

        self._enter_manual(gui)

    @staticmethod
    def _unregister_run_watcher(gui):
        gui.on_interp_state_got('EMC_TASK_INTERP_READING', None, None)
        gui.on_interp_state_got('EMC_TASK_INTERP_WAITING', None, None)

    @staticmethod
    def _register_run_watcher(gui):
        def program_started(_):
            ManualPrepareControlState._unregister_run_watcher(gui)
            gui.switch_state('RUNNING')

        gui.on_interp_state_got('EMC_TASK_INTERP_READING', program_started, None)
        gui.on_interp_state_got('EMC_TASK_INTERP_WAITING', program_started, None)

    def _cancel(self, gui):
        def disable_start(on_ready):
            print("_cancel::disable_start()")
            gui.machine_set_start_enabled(True, on_ready)

        def lock_axis(on_ready):
            print("_cancel::lock_axis()")
            return lambda _=None: gui.output_control_nonblock({self.enable_disabler_do_pin: True}, on_ready)

        #def home_X(on_ready):
        #    print("_cancel::home_X()")
        #    return lambda _=None: gui.home_axis_wait('X', on_ready)

        def disable_lube(on_ready):
            print("_cancel::disable_lube()")

            def _dis(_=None):
                gui.MDI_nonblock('M9', on_ready)

            return lambda _=None: task.deferLater(gui.get_reactor(), 0.5, _dis, None)

        def goto_home_state():
            print("_cancel::goto_home_state()")
            return lambda _=None: gui.switch_state('HOME_SCREEN')

        ManualPrepareControlState._unregister_run_watcher(gui)

        disable_start(
            lock_axis(
                disable_lube(
                    goto_home_state()
                )
            )
        )

    def _enter_manual(self, gui):
        def free_axis(on_ready):
            print("_enter_manual::free_axis()")
            gui.output_control_nonblock({self.enable_disabler_do_pin: False}, on_ready)

        def enable_start(on_ready=None):
            print("_enter_manual::enable_start()")
            return lambda _=None: gui.machine_set_start_enabled(True, on_ready)

        free_axis(
            enable_start()
        )

    def _lube_toggle(self, gui):
        def restore_watcher(_=None):
            ManualPrepareControlState._register_run_watcher(gui)
            gui.release_interface()

        def shedule_restore_watcher(_=None):
            task.deferLater(gui.get_reactor(), 2, restore_watcher, None)

        gui.hold_interface(None)
        ManualPrepareControlState._unregister_run_watcher(gui)

        self.lube_state = not self.lube_state
        gui.MDI_nonblock('M8' if self.lube_state else 'M9', on_ready=shedule_restore_watcher)

    def update_screen(self, gui):
        pass
