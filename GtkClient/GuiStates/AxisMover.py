# -*- coding: utf-8 -*-

from GuiStates import button7, button9
from SingleValueEditor import SingleValueEditor
from EditorButtons import EditorButtons


class AxisMover(SingleValueEditor, EditorButtons):
    def __init__(self, axis_name, step, parent_state, prompt, follower, units=''):
        self.axis_name = axis_name
        self.follower = follower
        self.newpos = None
        super(AxisMover, self).__init__(None, step, parent_state, prompt, units)

    def apply(self, gui):
        self.gui = gui

        EditorButtons.apply(self, gui)

        gui.set_button_text(button9, u'Назад')
        gui.set_button_enabled(button9, True)
        gui.set_button_callback(button9, self.close_editor, None)

        gui.set_button_text(button7, u'Домой')
        gui.set_button_enabled(button7, True)
        gui.set_button_callback(button7, self._home_axis, None)

        gui.clear_central_field()
        gui.add_central_field(self.widgets)

        gui.install_settings_change_hook(self.hook)

        self.update_screen(gui)

    def change_value(self, step):
        current_pos = self.gui.get_current_pos_by_axis(self.axis_name)
        self.newpos = round((current_pos + step) * 1000) / 1000.0
        self.follower.changed(self.axis_name, current_pos, self.newpos)
        self.update_screen(self.gui)

    def _home_axis(self, _):
        self.gui.home_axis_wait(self.axis_name)

    def update_screen(self, gui):
        if self.newpos is not None:
            v = self.newpos
        elif gui:
            v = gui.get_current_pos_by_axis(self.axis_name)
        else:
            v = 100

        self.value_display.set_markup(self.markup.format(
            v, self.units))
