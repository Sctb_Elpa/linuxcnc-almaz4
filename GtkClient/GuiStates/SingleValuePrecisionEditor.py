# -*- coding: utf-8 -*-

from GuiStates import button6, button1, button3, gen_value_markup
from SingleValueEditor import SingleValueEditor
from EditorButtons import EditorButtons


class SingleValuePrecisionEditor(SingleValueEditor):
    def __init__(self, value_name, step, step_precision, parent_state, editor_prompt, units='', hook=None):
        SingleValueEditor.__init__(self, value_name, step, parent_state, editor_prompt, units, hook)
        self.step_precision = step_precision
        self.markup = gen_value_markup(step_precision)

    def _apply_precision_buttons(self, gui):
        gui.set_button_text(button1, u'- {}'.format(self.step_precision))
        gui.set_button_enabled(button1, True)
        gui.set_button_callback(button1, self.change_value, -self.step_precision)
        gui.set_button_repeat_on_holding(button1, True)

        gui.set_button_text(button3, u'+ {}'.format(self.step_precision))
        gui.set_button_enabled(button3, True)
        gui.set_button_callback(button3, self.change_value, self.step_precision)
        gui.set_button_repeat_on_holding(button6, True)

    def apply(self, gui):
        EditorButtons.apply(self, gui)
        self._apply_precision_buttons(gui)
        self._apply_common(gui)
        self.update_screen(gui)