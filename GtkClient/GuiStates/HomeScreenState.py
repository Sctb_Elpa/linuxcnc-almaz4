# -*- coding: utf-8 -*-

from IGuiState import IGuiState
from GtkClient.Point import Point
from GuiStates import button7, button9, button4, button6, button1, button3
from CentralDescription import CentralDescription


class HomeScreenState(IGuiState):
    work_mode = 'Work Mode'

    state_defs = {
        button4: {'text': u'Подводка', 'callback_info': ('switch_state', 'MANUAL_PREPARE')},
        button9: {'text': u'Режим', 'callback_info': ('change_settings', work_mode)},
        button1: {'text': u'Настройка', 'callback_info': ('switch_state', 'SETTINGS')},
        button3: {'text': u'Диагностика', 'callback_info': ('switch_state', 'DIAGNOSTIC')}
    }

    def __init__(self):
        self.gui = None
        self.widgets = []
        w = IGuiState.create_gtk_label()
        w.set_markup(
            u"""<span size='xx-large' foreground='#0091ea'>Установка резки кварца Алмаз 4</span>""")
        self.widgets.append((w, Point(150, 70)))

        self.mode_label = IGuiState.create_gtk_label()
        self.widgets.append((self.mode_label, Point(230, 100)))

        self._ready_lbl = IGuiState.create_gtk_label()
        self._ready_lbl.hide()
        self._ready_lbl.set_markup(
            u"""<span size='x-large' foreground='#f44336'>ГОТОВ. Нажмите кнопку "Старт" для начала работы</span>""")
        self.widgets.append((self._ready_lbl, Point(100, 150)))

        self.content = CentralDescription()

    def _start_edit_cut_depth(self, _=None):
        self.gui.edit_single_value_precision(('Cutting depth', 2, 0.1, "HOME_SCREEN",
                                              u"<span size='xx-large'>Глубина реза</span>",
                                              u"мм"))

    def _disable(self, gui):
        def disable_machine(_=None):
            self.gui.machine_start_program("DisableMachine", lambda: self.gui.switch_state("DISABLED"))

        HomeScreenState._unregister_run_watcher(gui)
        self.gui.machine_set_start_enabled(False, disable_machine)

    def _start_prepare_program(self, gui):

        def enable_start_btn(_=None):
            gui.machine_set_start_enabled(True, gui.release_interface)

        def start_load_gcode(_=None):
            gui.machine_prepare_work_program(enable_start_btn)

        gui.hold_interface(None)
        gui.machine_set_start_enabled(False, start_load_gcode)

    def apply(self, gui):
        self.gui = gui
        gui.reset_buttons()

        IGuiState.common_apply(HomeScreenState.state_defs, self.gui)

        self.gui.set_button_text(button7, u'Выключить')
        self.gui.set_button_enabled(button7, True)
        self.gui.set_button_callback(button7, self._disable, gui)

        self.gui.set_button_text(button6, u'Глубина реза (W)')
        self.gui.set_button_enabled(button6, True)
        self.gui.set_button_callback(button6, self._start_edit_cut_depth, None)

        self.gui.clear_central_field()
        self.gui.add_central_field(self.widgets)
        self.content.attach(gui)

        HomeScreenState._register_run_watcher(gui)

        self.update_screen(self.gui)

    @staticmethod
    def _unregister_run_watcher(gui):
        gui.on_interp_state_got('EMC_TASK_INTERP_READING', None, None)
        gui.on_interp_state_got('EMC_TASK_INTERP_WAITING', None, None)

    @staticmethod
    def _register_run_watcher(gui):
        def program_started(_):
            HomeScreenState._unregister_run_watcher(gui)
            gui.switch_state('RUNNING')

        gui.on_interp_state_got('EMC_TASK_INTERP_READING', program_started, None)
        gui.on_interp_state_got('EMC_TASK_INTERP_WAITING', program_started, None)

    def release(self, gui):
        HomeScreenState._unregister_run_watcher(gui)
        gui.machine_set_start_enabled(False)
        pass

    def update_screen(self, gui):
        work_mode = gui.get_settings_value('Work Mode')
        mode_name = IGuiState.work_modes_table[work_mode]
        self.mode_label.set_markup(
            u"<span size='x-large'>Режим работы: </span>{}".format(mode_name))

        self.content.update(gui)

        self._start_prepare_program(gui)
