# -*- coding: utf-8 -*-

from IGuiState import IGuiState
from GuiStates import button7


class WorkingState(IGuiState):
    def apply(self, gui):
        gui.reset_buttons(button7)
        gui.set_button_text(button7, u'Остановить')
        gui.set_button_enabled(button7, True)
        gui.set_button_callback(button7, self.cancel_working, gui)

        gui.on_interp_state_got('EMC_TASK_INTERP_IDLE', self.on_program_finished, gui)

        gui.machine_start_program('Work', None)

    def update_screen(self, gui):
        pass

    def cancel_working(self, gui):
        gui.machine_interrupt_program(lambda: gui.switch_state("HOME_SCREEN"))

    def on_program_finished(self, gui):
        gui.on_interp_state_got('EMC_TASK_INTERP_IDLE', None)
        gui.switch_state("HOME_SCREEN")
