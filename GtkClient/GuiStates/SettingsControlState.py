# -*- coding: utf-8 -*-

from GtkClient.Point import Point

from IGuiState import IGuiState
from GuiStates import button7, button9, button4, button6, button1, button3
from CentralDescription import CentralDescription


class SettingsControlState(IGuiState):
    state_defs = {
        button7: dict(text=u'Подача (F)',
                      callback_info=('edit_single_value_precision',
                                     ('G1 speed', 0.1, 0.01, "SETTINGS",
                                      u"<span size='x-large'>Скорость рабочего хода при резании</span>", u'мм/мин'))),
        button4: dict(text=u'Оборотов на пластину',
                      callback_info=('edit_single_value',
                                     ('YR turns pre plate', 1, "SETTINGS",
                                      u"<span size='x-large'>Количество оборотов оси Y На 1 пластину</span>", ''))),
        button1: dict(text=u'Количество пластин',
                      callback_info=('edit_single_value',
                                     ('Plate count', 1, "SETTINGS",
                                      u"<span size='x-large'>Количество отрезаемых пластин</span>", u''))),
        button3: {'text': u'Выход', 'callback_info': ('switch_state', "HOME_SCREEN")}
    }

    def __init__(self):
        self.widgets = []
        self.widgets = []
        w = IGuiState.create_gtk_label()
        w.set_markup(u"<span size='xx-large' foreground='#0091ea'>Настройка</span>")
        self.widgets.append((w, Point(300, 70)))

        self.content = CentralDescription()

    @staticmethod
    def _append_toggle_state(gui, button, key):
        gui.force_toggle_button_state(button, gui.get_settings_value(key) != 0)

    def apply(self, gui):
        gui.reset_buttons()
        IGuiState.common_apply(SettingsControlState.state_defs, gui)

        gui.set_button_text(button9, u'СОЖ включена')
        gui.set_button_enabled(button9, True)
        gui.set_button_toggle_on_click(button9, True)
        self._append_toggle_state(gui, button9, 'Lube enabled')
        gui.set_button_callback(button9, SettingsControlState._lube_toggle, gui)

        gui.set_button_text(button6, u'Шпиндель включен')
        gui.set_button_enabled(button6, True)
        gui.set_button_toggle_on_click(button6, True)
        self._append_toggle_state(gui, button6, 'Spindel Enabled')
        gui.set_button_callback(button6, SettingsControlState._spindel_toggle, gui)

        gui.clear_central_field()
        gui.add_central_field(self.widgets)
        self.content.attach(gui)
        self.update_screen(gui)

    def update_screen(self, gui):
        self.content.update(gui)

    @staticmethod
    def _lube_toggle(gui):
        gui.change_settings(('Lube enabled', 1))

    @staticmethod
    def _spindel_toggle(gui):
        gui.change_settings(('Spindel Enabled', 1))
