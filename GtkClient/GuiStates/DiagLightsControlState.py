# -*- coding: utf-8 -*-

from IGuiState import IGuiState
from GtkClient.Point import Point
from GuiStates import button7, button4, button3


class DiagLightsControlState(IGuiState):
    state_defs = {
        button3: {'text': u'Выход', 'callback_info': ('switch_state', "DIAGNOSTIC")}
    }

    def __init__(self):
        self.widgets = []

        w = IGuiState.create_gtk_label()
        w.set_markup(u"<span size='xx-large' foreground='#0091ea'>Диагностика светофора</span>")
        self.widgets.append((w, Point(200, 100)))

        self.red_led = False
        self.green_led = False

    def apply(self, gui):
        self.red_led = False
        self.green_led = False

        gui.reset_buttons()
        IGuiState.common_apply(DiagLightsControlState.state_defs, gui)

        gui.set_button_text(button7, u'Красный')
        gui.set_button_enabled(button7, True)
        gui.set_button_callback(button7, self._red_led_toggle, gui)
        gui.set_button_toggle_on_click(button7, True)

        gui.set_button_text(button4, u'Зеленый')
        gui.set_button_enabled(button4, True)
        gui.set_button_callback(button4, self._green_led_toggle, gui)
        gui.set_button_toggle_on_click(button4, True)

        gui.clear_central_field()
        gui.add_central_field(self.widgets)

    @staticmethod
    def _set_led_state(gui, ctl):
        gui.output_control_nonblock(ctl)

    def _green_led_toggle(self, gui):
        self.green_led = not self.green_led
        DiagLightsControlState._set_led_state(gui, {gui.get_dio_by_name('led_green'): self.green_led})

    def _red_led_toggle(self, gui):
        self.red_led = not self.red_led
        DiagLightsControlState._set_led_state(gui, {gui.get_dio_by_name('led_red'): self.red_led})

    def release(self, gui):
        DiagLightsControlState._set_led_state(gui, {
            gui.get_dio_by_name('led_green'): False,
            gui.get_dio_by_name('led_red'): False
        })

    def update_screen(self, gui):
        pass
