# -*- coding: utf-8 -*-

from IGuiState import IGuiState
from GtkClient.Point import Point
from GuiStates import button7, button9, button4, button6, button1, button3, gen_value_markup


class DoubleValueEditor(IGuiState):
    def __init__(self, value1_name, step1, value2_name, step2, parent_state, editor_prompt, units1='', units2='',
                 hook=None):
        self.hook = hook
        self.value1_name = value1_name
        self.step1 = step1
        self.units1 = units1
        self.value2_name = value2_name
        self.step2 = step2
        self.units2 = units2

        self.parent_state = parent_state
        self.gui = None

        self.widgets = []

        self.editor_prompt = IGuiState.create_gtk_label()
        self.editor_prompt.set_markup(editor_prompt)
        self.value1_display = IGuiState.create_gtk_label()
        self.value2_display = IGuiState.create_gtk_label()

        self.markup1 = gen_value_markup(step1)
        self.markup2 = gen_value_markup(step2)

        self.update_screen(None)
        self.widgets.append((self.editor_prompt, Point(-0.5, 150)))
        self.widgets.append((self.value1_display, Point(-0.5, -0.5)))
        self.widgets.append((self.value2_display, Point(-0.5, -0.83)))

    def apply(self, gui):
        self.gui = gui
        gui.reset_buttons()

        gui.set_button_text(button7, u'Отмена')
        gui.set_button_enabled(button7, True)
        gui.set_button_callback(button7, self.cancel_edit,
                                (gui.get_settings_value(self.value1_name),
                                 gui.get_settings_value(self.value2_name)))

        gui.set_button_text(button9, u'ОК')
        gui.set_button_enabled(button9, True)
        gui.set_button_callback(button9, self.close_editor, None)

        gui.set_button_text(button4, u'- {}'.format(self.step1))
        gui.set_button_enabled(button4, True)
        gui.set_button_callback(button4, self.change_value1, -self.step1)
        gui.set_button_repeat_on_holding(button4, True)

        gui.set_button_text(button6, u'+ {}'.format(self.step1))
        gui.set_button_enabled(button6, True)
        gui.set_button_callback(button6, self.change_value1, self.step1)
        gui.set_button_repeat_on_holding(button6, True)

        gui.set_button_text(button1, u'- {}'.format(self.step1))
        gui.set_button_enabled(button1, True)
        gui.set_button_callback(button1, self.change_value2, -self.step1)
        gui.set_button_repeat_on_holding(button1, True)

        gui.set_button_text(button3, u'+ {}'.format(self.step2))
        gui.set_button_enabled(button3, True)
        gui.set_button_callback(button3, self.change_value2, self.step2)
        gui.set_button_repeat_on_holding(button3, True)

        gui.clear_central_field()
        gui.add_central_field(self.widgets)

        gui.install_settings_change_hook(self.hook)

        self.update_screen(gui)

    def cancel_edit(self, val):
        self.gui.set_settings_value(self.value1_name, val[0])
        self.gui.set_settings_value(self.value2_name, val[1])
        self.close_editor()

    def close_editor(self, dummy=0):
        self.gui.switch_state(self.parent_state)

    def change_value1(self, step):
        self.gui.change_settings((self.value1_name, step))

    def change_value2(self, step):
        self.gui.change_settings((self.value2_name, step))

    def update_screen(self, gui):
        if not gui:
            v1 = 100
            v2 = 100
        else:
            v1 = gui.get_settings_value(self.value1_name)
            v2 = gui.get_settings_value(self.value2_name)

        self.value1_display.set_markup(self.markup1.format(v1, self.units1))
        self.value2_display.set_markup(self.markup2.format(v2, self.units2))

    def release(self, gui):
        IGuiState.release(self, gui)
        gui.install_settings_change_hook(None)

