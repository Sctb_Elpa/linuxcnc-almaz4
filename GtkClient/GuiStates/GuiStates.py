# -*- coding: utf-8 -*-

import gtk
import os


from GtkClient.FollowHooks import FollowXYHook, FollowAxisHook
from GtkClient.Point import Point

from IGuiState import IGuiState

button7 = 'Upper-left button'
button4 = 'Mid-left button'
button1 = 'Down-left button'
button9 = 'Upper-right button'
button6 = 'Mid-right button'
button3 = 'Down-right button'

btn2_gtk_btn_id_map = {
    button7: 'button00',
    button9: 'button01',
    button4: 'button10',
    button6: 'button11',
    button1: 'button20',
    button3: 'button21',
}

btn2_pin_map = {
    button7: 7,
    button9: 9,
    button4: 4,
    button6: 6,
    button1: 1,
    button3: 3,
}

buttons = btn2_gtk_btn_id_map.keys()

lube_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'img/Almaz_lube.png')
no_lube = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'img/Almaz_no_lube.png')

almaz_lube_400x270 = gtk.gdk.pixbuf_new_from_file_at_size(lube_path, 400, 270)
almaz_no_lube_400x270 = gtk.gdk.pixbuf_new_from_file_at_size(no_lube, 400, 270)


def gen_value_markup(step):
    acuracy = int(('%E' % step).split('E')[1])
    if acuracy < 0:
        return u"<span size='xx-large'>{} </span><span size='large'>{}</span>".format(
            '{:.' + str(-acuracy) + 'f}', '{}')
    else:
        return u"<span size='xx-large'>{:.0f} </span><span size='large'>{}</span>"

