# -*- coding: utf-8 -*-

from GuiStates import button4, button6


class EditorButtons:
    def apply(self, gui):
        gui.reset_buttons()

        gui.set_button_text(button4, u'- {}'.format(self.step))
        gui.set_button_enabled(button4, True)
        gui.set_button_callback(button4, self.change_value, -self.step)
        gui.set_button_repeat_on_holding(button4, True)

        gui.set_button_text(button6, u'+ {}'.format(self.step))
        gui.set_button_enabled(button6, True)
        gui.set_button_callback(button6, self.change_value, self.step)
        gui.set_button_repeat_on_holding(button6, True)

    def change_value(self, step):
        pass
