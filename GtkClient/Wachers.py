# -*- coding: utf-8 -*-


from abc import ABCMeta, abstractmethod
from collections import OrderedDict


class IWatcher:
    __metaclass__ = ABCMeta

    @abstractmethod
    def on_message(self, message):
        """
        :param message: watch message
        """


class UnknownWatcher(IWatcher):
    def on_message(self, message):
        print 'Unknown watch message type: {}'.format(message['id'])


class PositionWatcher(IWatcher):
    def __init__(self, status_bar):
        self._status_bar = status_bar
        self._status_bar.set_use_markup(True)
        self.current_pos = [0.0]
        self.on_pos_changed = []
        self.update_status_bar()

    def on_message(self, message):
        if message[u'code'] == u'?OK':
            self.current_pos = message[u'data']
            self.update_status_bar()
            self.inform_subscribers()

    def update_status_bar(self):
        self._status_bar.set_markup(u"\
<span foreground='#f44336'>X: </span><span foreground='#607d8b' size='large'>{:.2f}</span>".format(
            *self.current_pos))

    def inform_subscribers(self):
        for cb in self.on_pos_changed:
            cb(self.current_pos)

    def current_pos(self, axis):
        return self.current_pos[axis]


class DIWatcher(IWatcher):
    def __init__(self, buttons, yr_indicator, _map):
        """
        :param buttons:
        :param _map: словарь вида {"button_name": pin_number, ...}
        """
        self.buttons = buttons
        self._yr_indicator = yr_indicator
        self._map = _map
        self.current_btn_state = dict.fromkeys(_map.keys(), 0)
        self.prev_homes_state = [False]
        self.prev_yr_state = False
        self.on_yr_changed = []

    def on_message(self, message):
        if message[u'code'] == u'?OK':
            new_btn_state = dict.fromkeys(self._map.keys(), 0)
            data = message[u'data']
            for key, value in self._map.iteritems():
                new_btn_state[key] = data[value]

            changed = self.find_first_change(new_btn_state)
            if changed:
                newstate = new_btn_state[changed]
                self.buttons[changed].changed(None, newstate)
                self.current_btn_state = new_btn_state

            self.print_homes_state(data)
            self.update_yr_state(data)

    def print_homes_state(self, d):
        hs = [d[10]]
        if hs != self.prev_homes_state:
            self.prev_homes_state = hs
            print 'New homes state: {}'.format(hs)

    def update_yr_state(self, d):
        new_yr_state = d[11]
        if new_yr_state != self.prev_yr_state:
            self.prev_yr_state = new_yr_state
            print 'New rotation sensor state: {}'.format(new_yr_state)
            self._yr_indicator.is_on = new_yr_state

            for cb in self.on_yr_changed:
                cb(new_yr_state)

    def find_first_change(self, newstate):
        for key, value in self.current_btn_state.iteritems():
            if newstate[key] != value:
                return key
        return None


class InterpretatorStateWatcher(IWatcher):
    def __init__(self):
        self.interp_state = "EMC_TASK_INTERP_IDLE"
        self.on_interp_state = OrderedDict()
        self.on_interp_state["EMC_TASK_INTERP_IDLE"] = (None, None)
        self.on_interp_state["EMC_TASK_INTERP_READING"] = (None, None)
        self.on_interp_state["EMC_TASK_INTERP_PAUSED"] = (None, None)
        self.on_interp_state["EMC_TASK_INTERP_WAITING"] = (None, None)

    def on_message(self, message):
        if message[u'data'] == 'Server is not running.':
            self.interp_state = "EMC_TASK_INTERP_IDLE"
        else:
            self.interp_state = self.on_interp_state.keys()[int(message[u'data']) - 1]
        print "Interp state changed to {}".format(self.interp_state)

        callback, cookie = self.on_interp_state[self.interp_state]

        if callback:
            callback(cookie)

    def on_got_state(self, state, callback, cookie=None):
        self.on_interp_state[state] = (callback, cookie)


class EStopWatcher(IWatcher):
    def __init__(self, on_estop):
        self.estop = True
        self.on_estop = on_estop

    def on_message(self, message):
        if message[u'code'] == u'?OK':
            data = message[u'data']
            print('Estop={}'.format(data))
            self.estop = data != 0
            if self.estop:
                self.on_estop()

    def is_estop(self):
        return self.estop
