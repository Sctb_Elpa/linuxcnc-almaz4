# -*- coding: utf-8 -*-

from twisted.internet import task

from IMachineSequence import IMachineSequence


class SystemShutdown(IMachineSequence):
    def __init__(self, controller, before):
        self.controller = controller
        self.beforecb = before

    def run(self, arg):
        d = self._shutdown_linuxcnc(None)
        d.addCallback(self._shutdown_linuxcnc)
        return d, arg

    def _shutdown_linuxcnc(self, _):
        def shadule_poweroff(_):
            task.deferLater(self.controller.get_reactor(), 10, self._system_shutdown, None)

        d = self.controller.send_msg({"command": "put", "name": "shutdown"})
        d.addCallback(shadule_poweroff)
        return d

    def _system_shutdown(self, _):
        import os
        self.beforecb()
        os.system('sync && sudo poweroff')
        self.controller.get_reactor().stop()
        print "Powering machine off"

