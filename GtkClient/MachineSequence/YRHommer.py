# -*- coding: utf-8 -*-

from twisted.internet import task

from IMachineSequence import IMachineSequence
from MachineStatusWaiter import MachineStatusWaiter


class YRHommer(IMachineSequence):
    def __init__(self, controller, yr_pin, yr_sensor_pin):
        self.controller = controller
        self.yr_pin = yr_pin
        self.yr_sensor_pin = yr_sensor_pin
        self.machine_waiter = MachineStatusWaiter(controller)

    def generate_gcode(self, _=None):
        return """%
M66 P{yr_sensor_pin} L0 (get Y sensor value to #5399)
o100 if [#5399 EQ 0] (if #5399 == 0, not at sensor)
    M64 P{yr_pin} (start rotation)
    (wait rotation ends)
    o110 do
        G4 P.05 (pause 0.05s)
        M66 P{yr_sensor_pin} L0 (Read sensor pin L0 -> #5399 = sensor)
    o110 while [#5399 EQ 0] (sensor == 0 rotation continues)
    M65 P{yr_pin}
o100 endif
%
""".format(yr_sensor_pin=self.yr_sensor_pin, yr_pin=self.yr_pin)

    def _load_gcode(self, g_code):
        return self.controller.send_msg({"command": "put", "name": "program_upload", '0': 'gcode.cnc', '1': g_code})

    def execute_process(self, _):
        return self.controller.send_msg({"command": "put", "name": "auto", '0': "AUTO_RUN", "1": 0})

    def run(self, on_success):
        def sleep_gen(s):
            return lambda _=None: task.deferLater(self.controller.get_reactor(), s, lambda _: None, None)

        def _mdi(_=None):
            return self.controller.change_mode('MODE_AUTO')

        d = _mdi()
        d.addCallback(self.generate_gcode)
        d.addCallback(self._load_gcode)
        d.addCallback(sleep_gen(2))
        d.addCallback(self.execute_process)
        d.addCallback(self.machine_waiter.wait_machine_stop)
        d.addCallback(_mdi)
        return d, on_success
