# -*- coding: utf-8 -*-

from IMachineSequence import IMachineSequence
from StateSwitcher import StateSwitcher
from MachineReseter import MachineReseter


class DisableMachineSequence(IMachineSequence):
    def __init__(self, controller, settings, yr_pin, red_led_pin, green_led_pin):
        self.state_switcher = StateSwitcher(controller)
        self.reseter = MachineReseter(controller, settings, yr_pin, red_led_pin, green_led_pin)

    def run(self, arg):
        d, _ = self.reseter.run(None)
        d.addCallback(self._machine_disable)
        return d, arg

    def _machine_disable(self, _):
        return self.state_switcher.switch_machine_state('STATE_ESTOP')

