# -*- coding: utf-8 -*-


class StateSwitcher:
    def __init__(self, controller):
        self.controller = controller

    def switch_machine_state(self, state):
        return self.controller.send_msg({'command': 'put', 'name': 'state', '0': state})
