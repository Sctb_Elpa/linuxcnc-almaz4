# -*- coding: utf-8 -*-

from twisted.internet import task

from MachineStatusWaiter import MachineStatusWaiter
from ConfigRequster import ConfigRequster
from YRHommer import YRHommer


class Homer:
    def __init__(self, controller, settings, yr_pin, yr_sensor_pin):
        self.controller = controller
        self.homing_order = None
        self.order_pos = 0
        self.config_requester = ConfigRequster(controller, settings)
        self.stop_waiter = MachineStatusWaiter(controller)
        self.yrHommer = YRHommer(controller, yr_pin, yr_sensor_pin)

    def machine_home(self, _):
        print "Homing machine..."
        d = self.config_requester.get_config()
        d.addCallback(self._generate_homing_order)
        d.addCallback(self._home_execute)
        d.addCallback(self.y_home)
        return d

    def _generate_homing_order(self, _):
        axes_count = int(self.config_requester.get_config_value(u'TRAJ', u'AXES'))

        homming_sequence = {}
        for axis in xrange(axes_count):
            try:
                homming_sequence[axis] = int(self.config_requester.get_config_value(u'AXIS_{}'.format(axis),
                                                                                    u'HOME_SEQUENCE'))
            except KeyError:
                homming_sequence[axis] = -1

        def sort_home_sequence(seq):
            steps = sorted(seq.values())
            order = {}
            for step in steps:
                order[step] = [key for key, val in seq.iteritems() if val == step]
            return order

        self.homing_order = sort_home_sequence(homming_sequence)
        print "Homing order: {}".format(self.homing_order)
        self.order_pos = 0

    def _home_execute(self, _):
        def send_home_cmd(_):
            for axis in self.homing_order[sorted(self.homing_order.keys())[self.order_pos]]:
                print "Sending home axis {} command".format(axis)
                self.controller.send_msg_no_reply({"command": "put", "name": "home", "0": str(axis)})

        def ready(_):
            print "Homing stage {} passed".format(self.order_pos)
            self.order_pos += 1
            if self.order_pos < len(self.homing_order):
                return task.deferLater(self.controller.get_reactor(), 0.5, self._home_execute, None)
            else:
                print "All axes homed!"

        def sleep_(s):
            return lambda _=None: task.deferLater(self.controller.get_reactor(), s, lambda _: None, None)

        print "Homing pos: {}".format(self.order_pos)
        d = self.controller.change_mode('MODE_MANUAL')
        d.addCallback(self.stop_waiter.wait_machine_stop)
        d.addCallback(send_home_cmd)
        d.addCallback(sleep_(2)) #d.addCallback(self.stop_waiter.wait_machine_start)
        d.addCallback(self.stop_waiter.wait_machine_stop)
        d.addCallback(ready)
        return d

    def y_home(self, _):
        dt, _ = self.yrHommer.run(None)
        return dt
