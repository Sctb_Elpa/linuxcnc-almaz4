# -*- coding: utf-8 -*-

from abc import ABCMeta, abstractmethod


class IMachineSequence:
    __metaclass__ = ABCMeta

    @abstractmethod
    def run(self, arg):
        pass
