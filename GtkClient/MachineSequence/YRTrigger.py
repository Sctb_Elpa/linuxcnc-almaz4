# -*- coding: utf-8 -*-

from twisted.internet import task

from IMachineSequence import IMachineSequence
from DIWaiter import DIWaiter


class YRTrigger(IMachineSequence):
    def __init__(self, controller, yr_pin, yr_sensor_pin):
        self.controller = controller
        self.yr_pin = yr_pin
        self.waiter = DIWaiter(controller, yr_sensor_pin, 1)

    def run(self, on_success):
        def _pin_ctl(cmd):
            return lambda _=None: self.controller.send_msg(
                {"command": "put", "name": "mdi", '0': '{} P{}'.format(cmd, self.yr_pin)})

        def sleep(delay):
            return lambda _=None: task.deferLater(self.controller.get_reactor(), delay, lambda: None)

        def _process_mdi(_):
            _d = _pin_ctl('M64')()
            _d.addCallback(sleep(1))
            _d.addCallback(self.waiter.wait_pin)
            _d.addCallback(_pin_ctl('M65'))
            return _d

        d = self.controller.change_mode('MODE_MDI')
        d.addCallback(_process_mdi)
        return d, on_success
