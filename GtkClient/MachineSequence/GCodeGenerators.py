# -*- coding: utf-8 -*-

import StringIO
from abc import ABCMeta, abstractmethod


class ICodeGenerator:
    __metaclass__ = ABCMeta

    def __init__(self):
        self.code = StringIO.StringIO()

    @abstractmethod
    def generate(self):
        pass

    def getcode(self):
        if self.code.len == 0:
            self.generate()
        return self.code.getvalue()


class InitialSequenceGenerator(ICodeGenerator):
    def __init__(self, is_spindel_enabled, is_lube_enabled, red_led_pin, green_led_pin, enable_disabler_do_pin, startup_delay=0):
        ICodeGenerator.__init__(self)
        self.is_spindel_enabled = is_spindel_enabled
        self.is_lube_enabled = is_lube_enabled
        self.red_led_pin = red_led_pin
        self.green_led_pin = green_led_pin
        self.startup_delay = startup_delay
        self.enable_disabler_do_pin = enable_disabler_do_pin

    def generate(self):
        self.code.write('%\n')
        self.code.write('M65 P{}\n'.format(self.green_led_pin))
        self.code.write('M64 P{}\n'.format(self.red_led_pin))
        self.code.write('M64 P{}\n'.format(self.enable_disabler_do_pin))
        if self.is_spindel_enabled:
            self.code.write('M3 S1\n')
        else:
            self.code.write('M5\n')
        if self.is_lube_enabled:
            self.code.write('M8\n')
        else:
            self.code.write('M9\n')
        if self.startup_delay > 0:
            self.code.write('G4 P{}\n'.format(self.startup_delay))


class FinaliseSequenceGenerator(ICodeGenerator):
    def __init__(self, is_spindel_enabled, is_lube_enabled, red_led_pin, do_not_stop_spindel=False):
        ICodeGenerator.__init__(self)
        self.is_spindel_enabled = is_spindel_enabled
        self.is_lube_enabled = is_lube_enabled
        self.red_led_pin = red_led_pin
        self.do_not_stop_spindel = do_not_stop_spindel

    def generate(self):
        if self.is_lube_enabled:
            self.code.write('M9\n')

        if self.is_spindel_enabled and not self.do_not_stop_spindel:
            self.code.write('M5\n')

        self.code.write('M65 P{}'.format(self.red_led_pin))
        self.code.write('%')


class SimplePassGenerator(ICodeGenerator):
    def __init__(self, Cutting_depth, G1_speed):
        ICodeGenerator.__init__(self)
        self.Cutting_depth = Cutting_depth
        self.G1_speed = G1_speed

    def generate(self):
        self.code.write('G1 X{} F{}\n'.format(self.Cutting_depth, self.G1_speed))
        self.code.write('G0 X0\n')


class YRTriggerGenerator(ICodeGenerator):
    def __init__(self, yr_trigger_pin, yr_sensor_pin, y_turns):
        ICodeGenerator.__init__(self)
        self.yr_trigger_pin = yr_trigger_pin
        self.yr_sensor_pin = yr_sensor_pin
        self.y_turns = y_turns

    def generate_sub(self):
        return """o100 sub
    (Programm YR rotation)
    M64 P{pin} (Enable rotation)
    G4 P2 (wait 2 sec to leave sensor)
    M66 P{sensor} L0 (Read sensor pin L0 -> #5399 = sensor)
    o110 if [#5399 EQ 0] (sensor value 0 - rotation works)
        (wait rotation ends)
        o120 do
            G4 P.05 (pause 0.05s)
            M66 P{sensor} L0 (Read sensor pin L0 -> #5399 = sensor)
        o120 while [#5399 EQ 0] (sensor == 0 rotation continues) 
    o110 endif
    M65 P{pin} (disable rotation)
o100 endsub
""".format(pin=self.yr_trigger_pin, sensor=self.yr_sensor_pin)

    def generate(self):
        for turn in xrange(self.y_turns):
            self.code.write("o100 call\n")


class ManualSequence(ICodeGenerator):
    def __init__(self, is_spindel_enabled, is_lube_enabled, cutting_depth, G1_speed, red_led_pin, green_led_pin,
                 enable_disabler_do_pin, startup_delay=0):
        ICodeGenerator.__init__(self)
        self.initial_generator = InitialSequenceGenerator(is_spindel_enabled, is_lube_enabled, red_led_pin,
                                                          green_led_pin, enable_disabler_do_pin, startup_delay)
        self.finaliser = FinaliseSequenceGenerator(is_spindel_enabled, is_lube_enabled, red_led_pin,
                                                   do_not_stop_spindel=True)
        self.pass_generator = SimplePassGenerator(cutting_depth, G1_speed)

    def generate(self):
        self.code.write(self.initial_generator.getcode())
        self.code.write(self.pass_generator.getcode())
        self.code.write(self.finaliser.getcode())


class MultiSequence(ICodeGenerator):
    def __init__(self, is_spindel_enabled, is_lube_enabled, cutting_depth, G1_speed, red_led_pin, green_led_pin,
                 yr_trigger_pin, yr_sensor_pin, y_turns, enable_disabler_do_pin, plate_count=1, startup_delay=0):
        ICodeGenerator.__init__(self)
        self.initial_generator = InitialSequenceGenerator(is_spindel_enabled, is_lube_enabled, red_led_pin,
                                                          green_led_pin, enable_disabler_do_pin, startup_delay)
        self.finaliser = FinaliseSequenceGenerator(is_spindel_enabled, is_lube_enabled, red_led_pin)
        self.pass_generator = SimplePassGenerator(cutting_depth, G1_speed)
        self.yr_generator = YRTriggerGenerator(yr_trigger_pin, yr_sensor_pin, y_turns)
        self.plate_count = plate_count

    def generate(self):
        self.code.write(self.initial_generator.getcode())
        self.code.write(self.yr_generator.generate_sub())
        for plate in xrange(self.plate_count):
            self.code.write(self.pass_generator.getcode())
            if plate < self.plate_count - 1:
                self.code.write(self.yr_generator.getcode())

        self.code.write(self.finaliser.getcode())


class SingleSequence(MultiSequence):
    def __init__(self, is_spindel_enabled, is_lube_enabled, cutting_depth, G1_speed, red_led_pin, green_led_pin,
                 yr_trigger_pin, yr_sensor_pin, y_turns, enable_disabler_do_pin, plate_count=1, startup_delay=0):
        MultiSequence.__init__(self, is_spindel_enabled, is_lube_enabled, cutting_depth, G1_speed, red_led_pin,
                               green_led_pin, yr_trigger_pin, yr_sensor_pin, y_turns, enable_disabler_do_pin,
                               plate_count, startup_delay)

    def generate(self):
        self.code.write(self.initial_generator.getcode())
        self.code.write(self.pass_generator.getcode())
        self.code.write(self.yr_generator.getcode())
        self.code.write(self.finaliser.getcode())
