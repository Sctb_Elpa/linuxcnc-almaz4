# -*- coding: utf-8 -*-

from MDI_nonblock import MDI_nonblock
from MachineStatusWaiter import MachineStatusWaiter


class MDIWait(MDI_nonblock):
    def __init__(self, controller):
        MDI_nonblock.__init__(self, controller)
        self.waiter = MachineStatusWaiter(controller)

    def run(self, arg):
        self.mdi_str, on_success = arg
        print('MDIWait: {}'.format(self.mdi_str))
        d = self.controller.change_mode('MODE_MDI')
        d.addCallback(self._process_mdi)
        d.addCallback(self.waiter.wait_machine_stop)
        return d, on_success
