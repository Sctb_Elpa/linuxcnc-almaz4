# -*- coding: utf-8 -*-

from IMachineSequence import IMachineSequence
from GCodeGenerators import ManualSequence, SingleSequence, MultiSequence


class PrepareSequence(IMachineSequence):
    def __init__(self, controller, settings, red_led_pin, green_led_pin, yr_trigger_pin, yr_sensor_pin,
                 enable_disabler_do_pin, startup_delay=0):
        self.controller = controller
        self.settings = settings
        self.red_led_pin = red_led_pin
        self.green_led_pin = green_led_pin
        self.yr_trigger_pin = yr_trigger_pin
        self.yr_sensor_pin = yr_sensor_pin
        self.enable_disabler_do_pin = enable_disabler_do_pin
        self.startup_delay = startup_delay
        self.out_dict = None

    def _load_gcode(self, g_code):
        return self.controller.send_msg({"command": "put", "name": "program_upload", '0': 'gcode.cnc', '1': g_code})

    @staticmethod
    def _generate_pass(code, Cutting_depth, G1_speed):
        code.write('G1 X{} F{}\n'.format(Cutting_depth, G1_speed))
        code.write('G0 X0\n')

    def _generate_gcode(self):
        is_spindel_enabled = self.settings['Spindel Enabled']
        is_lube_enabled = self.settings['Lube enabled']
        G1_speed = self.settings['G1 speed']
        cutting_depth = self.settings['Cutting depth']
        plate_count = self.settings['Plate count']
        yr_turns_pre_plate = self.settings['YR turns pre plate']
        mode = self.settings['Work Mode']

        gen = None
        if mode == 0:
            gen = ManualSequence(is_spindel_enabled, is_lube_enabled, cutting_depth, G1_speed, self.red_led_pin,
                                 self.green_led_pin, self.enable_disabler_do_pin, self.startup_delay)
        elif mode == 1:
            gen = SingleSequence(is_spindel_enabled, is_lube_enabled, cutting_depth, G1_speed, self.red_led_pin,
                                 self.green_led_pin, self.yr_trigger_pin, self.yr_sensor_pin, yr_turns_pre_plate,
                                 self.enable_disabler_do_pin, plate_count, self.startup_delay)
        elif mode == 2:
            gen = MultiSequence(is_spindel_enabled, is_lube_enabled, cutting_depth, G1_speed, self.red_led_pin,
                                self.green_led_pin, self.yr_trigger_pin, self.yr_sensor_pin, yr_turns_pre_plate,
                                self.enable_disabler_do_pin, plate_count, self.startup_delay)

        return gen.getcode()

    def run(self, on_success):
        d = self._load_gcode(self._generate_gcode())
        return d, on_success
