# -*- coding: utf-8 -*-

from twisted.internet import task

from IMachineSequence import IMachineSequence


class StartupSequence(IMachineSequence):
    def __init__(self, controller):
        self.controller = controller

    def run(self, arg):
        d = self._start_linuxcnc(None)
        d.addCallback(self._machine_wait_start)
        d.addCallback(self.controller.get_current_mode)
        d.addCallback(self._register_watchers)

        return d, arg

    def _start_linuxcnc(self, _):
        print "Starting linuxcnc..."
        return self.controller.send_msg({"command": "put", "name": "startup"})

    def _machine_wait_start(self, _):
        def wait_linuxcnc_rdy(_):
            print "linuxcnc not ready, recheck..."
            return task.deferLater(self.controller.get_reactor(), 0.1, self._machine_wait_start, None)

        def sleep_2s(_):
            return task.deferLater(self.controller.get_reactor(), 2, lambda _: None, None)

        def check_result(msg):
            if msg['data'] != 1:
                raise

        print "Waiting for linuxcnc started..."
        d = self.controller.send_msg({"command": "get", "name": "running"})
        d.addCallback(check_result)
        d.addCallbacks(sleep_2s, wait_linuxcnc_rdy)
        return d

    def _register_watchers(self, _):
        def _register_watches_actual_pos(_):
            return self.controller.add_watch_list('actual_position')

        def _register_watches_pos(_):
            return self.controller.add_watch_list('din')

        def _register_interp_state(_):
            return self.controller.add_watch_list('interp_state')

        def _register_estop_state(_):
            return self.controller.add_watch_list('estop')

        d = _register_watches_actual_pos(None)
        d.addCallback(_register_watches_pos)
        d.addCallback(_register_interp_state)
        d.addCallback(_register_estop_state)
        return d
