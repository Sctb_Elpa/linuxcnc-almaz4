# -*- coding: utf-8 -*-

from IMachineSequence import IMachineSequence
from MachineStatusWaiter import MachineStatusWaiter
from StateSwitcher import StateSwitcher
from Homer import Homer
from MachineReseter import MachineReseter


class InitMachineSequence(IMachineSequence):
    def __init__(self, controller, settings, enable_hack_pin, red_led_pin, green_led_pin, yr_pin, yr_sensor_pin):
        self.state_switcher = StateSwitcher(controller)
        self.controller = controller
        self.homer = Homer(controller, settings, yr_pin, yr_sensor_pin)
        self.reseter = MachineReseter(controller, settings, yr_pin, red_led_pin, green_led_pin)
        self.machine_waiter = MachineStatusWaiter(controller)
        self.enable_hack_pin = enable_hack_pin

    def run(self, arg):
        d = self._machine_reset_estop(None)
        d.addCallback(self.homer.config_requester.get_config)
        d.addCallback(self._check_limits)
        d.addCallback(self._machine_enable)
        d.addCallback(self.reset_enable_hack)
        d.addCallback(self.homer.machine_home)
        d.addCallback(self.machine_waiter.wait_machine_stop)
        d.addCallback(self.reseter.reset_machine)
        return d, arg

    def _machine_reset_estop(self, _):
        print "Resetting ESTOP..."
        return self.state_switcher.switch_machine_state('STATE_ESTOP_RESET')

    def _machine_enable(self, _):
        print "Turning machine ON..."
        return self.state_switcher.switch_machine_state('STATE_ON')

    # читает лимиты из ini и записывает в self.homer.config_requester.settings
    def _check_limits(self, _):
        config_requester = self.homer.config_requester
        settings = self.homer.config_requester.settings
        axes_count = int(config_requester.get_config_value(u'TRAJ', u'AXES'))
        axes = ('X',)

        minimums = {
            axes[axis]: float(config_requester.get_config_value(u'AXIS_{}'.format(axis), u'MIN_LIMIT'))
            for axis in xrange(axes_count)}
        maximums = {
            axes[axis]: float(config_requester.get_config_value(u'AXIS_{}'.format(axis), u'MAX_LIMIT'))
            for axis in xrange(axes_count)}

        settings.set_axis_min_limit(**minimums)
        settings.set_axis_max_limit(**maximums)
        settings.save_settings()

    def reset_enable_hack(self, _):
        def reset_hack_mdi(_):
            self.controller.send_msg({"command": "put", "name": "mdi", '0': 'M64 P{}'.format(self.enable_hack_pin)})

        d = self.controller.change_mode('MODE_MDI')
        d.addCallback(reset_hack_mdi)
        return d
