# -*- coding: utf-8 -*-

from IMachineSequence import IMachineSequence


class MDI_nonblock(IMachineSequence):
    def __init__(self, controller):
        self.controller = controller
        self.mdi_str = ''

    def run(self, arg):
        self.mdi_str, on_success = arg
        print('MDI_nonblock: {}'.format(self.mdi_str))
        d = self.controller.change_mode('MODE_MDI')
        d.addCallback(self._process_mdi)
        return d, on_success

    def _process_mdi(self, _):
        return self.controller.send_msg_no_reply({"command": "put", "name": "mdi", '0': self.mdi_str})
