# -*- coding: utf-8 -*-

from IMachineSequence import IMachineSequence
from twisted.internet import defer


class MDI_D_Out_Sequence(IMachineSequence):
    def __init__(self, controller):
        self.controller = controller
        self.out_dict = None

    def run(self, arg):
        self.out_dict, on_success = arg
        print('MDI_D_Out_Sequence: {}'.format(self.out_dict))
        d = self.controller.change_mode('MODE_MDI')
        d.addCallback(self._process_mdi)
        return d, on_success

    def _process_mdi(self, _):
        def _gen_send_command_fun(mdi):
            return lambda _=None: self.controller.send_msg({"command": "put", "name": "mdi", '0': mdi_command})

        d = defer.Deferred()
        for output, newstate in self.out_dict.iteritems():
            mdi_command = 'M{} P{}'.format(64 if newstate else 65, output)
            d.addCallback(_gen_send_command_fun(mdi_command))

        d.callback(None)
        return d
