# -*- coding: utf-8 -*-

from IMachineSequence import IMachineSequence


class MDI_GO_Sequence(IMachineSequence):
    def __init__(self, controller):
        self.controller = controller
        self.axis_dict = None

    def run(self, arg):
        self.axis_dict, on_success = arg
        d = self.controller.change_mode('MODE_MDI')
        d.addCallback(self._process_mdi)
        return d, on_success

    def _process_mdi(self, _):
        mdi_command = "G0"
        for key, val in self.axis_dict.iteritems():
            mdi_command += " {}{}".format(key, val)
        return self.controller.send_msg_no_reply({"command": "put", "name": "mdi", '0': mdi_command})
