# -*- coding: utf-8 -*-

from twisted.internet import task
from twisted.internet.defer import CancelledError


class DIWaiter:
    def __init__(self, controller, di_pin, state):
        self.controller = controller
        self.di_pin = di_pin
        self.state = state

    def _send_req(self, _):
        return self.controller.send_msg({"command": "get", "name": "din"})

    def _check_answer(self, ans):
        if ans[u'data'][self.di_pin] != int(self.state):
            raise
        return True

    def get_pin(self, _=None):
        d = task.deferLater(self.controller.get_reactor(), 0, self._send_req, None)
        d.addCallback(self._check_answer)
        d.addErrback(lambda _: False)
        return d

    def wait_pin(self, _=None):
        print "Awaiting DI{:02d}...".format(self.di_pin)

        def schedule_recheck_machine_is_stops(err):
            if err and err.type == CancelledError:
                return err
            return task.deferLater(self.controller.get_reactor(), 0, self.wait_pin, None)

        d = task.deferLater(self.controller.get_reactor(), 0, self._send_req, None)
        d.addCallback(self._check_answer)
        d.addErrback(schedule_recheck_machine_is_stops)
        return d
