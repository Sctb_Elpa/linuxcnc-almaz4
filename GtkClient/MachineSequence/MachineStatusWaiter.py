# -*- coding: utf-8 -*-

from twisted.internet import task
from twisted.internet.defer import CancelledError


class MachineStatusWaiter:
    def __init__(self, controller):
        self.controller = controller

    def wait_machine_stop(self, _=None):
        print "Awaiting machine stops..."

        def schedule_recheck_machine_is_stops(err):
            if err and err.type == CancelledError:
                return err
            return task.deferLater(self.controller.get_reactor(), 0, self.wait_machine_stop, None)

        d = task.deferLater(self.controller.get_reactor(), 0, self._send_req, None)
        d.addCallback(self._check_answer)
        d.addErrback(schedule_recheck_machine_is_stops)
        return d

    def _send_req(self, _):
        return self.controller.send_msg({"command": "get", "name": "state"})

    def _check_answer(self, ans):
        if ans[u'data'] == 2:
            raise

    def wait_machine_start(self, _=None):
        print "Awaiting machine starts..."

        def schedule_recheck_machine_moving(err):
            if err and err.type == CancelledError:
                return err
            return task.deferLater(self.controller.get_reactor(), 0, self.wait_machine_start, None)

        d = task.deferLater(self.controller.get_reactor(), 0, self._send_req, None)
        d.addCallback(self._check_answer)
        d.addCallbacks(schedule_recheck_machine_moving, lambda _: None)
        return d
