# -*- coding: utf-8 -*-


class ConfigRequster:
    def __init__(self, controller, settings=None):
        self.settings = settings
        self.controller = controller
        self.config = None

    def get_config(self, _=None):
        d = self.controller.send_msg({"command": "get", "name": "config"})
        d.addCallback(self._process_config)
        return d

    def _process_config(self, config_json):
        self.config = config_json[u'data'][u'parameters']
        return self.config

    def home_pos(self, _=None):
        homes_nicknames = ('xh', 'yh', 'zh', 'ah', 'bh')
        hp = self.get_config_value(u'TRAJ', u'HOME').split(' ')
        return {homes_nicknames[i]: hp[i] for i in xrange(len(hp))}

    def get_config_value(self, section, key):
        def filter_fun(parameter):
            v = parameter[u'values']
            return v[u'section'] == section and v[u'name'] == key

        res = filter(filter_fun, self.config)
        if res:
            return res[0][u'values'][u'value']

        raise KeyError('No such parameter "[{}]/{}" in configuration'.format(section, key))

    def wash_pos(self, _=None):
        settings_h_values = [self.settings['mode_{}_H'.format(i)] for i in xrange(6)]
        return {'wp_X': self.settings['Washing place X'],
                'wp_Y': self.settings['Washing place Y'],
                'wp_Z': self.settings['Washing place Z'],
                'wp_B': self.get_config_value(u'AXIS_4', u'MAX_LIMIT')}

    def closed_B_pos(self, _=None):
        return float(self.get_config_value(u'AXIS_4', u'CLOSE_POS'))

    def B_hister(self, _=None):
        return float(self.get_config_value(u'AXIS_4', u'H_HISTER'))

