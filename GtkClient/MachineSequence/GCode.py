# -*- coding: utf-8 -*-

from abc import ABCMeta, abstractmethod
from IMachineSequence import IMachineSequence


class GCode(IMachineSequence):
    __metaclass__ = ABCMeta

    def __init__(self, controller):
        self.controller = controller

    def set_auto_mode(self, _):
        return self.controller.change_mode('MODE_AUTO')

    def run(self, on_success):
        d = self.set_auto_mode(None)
        d.addCallback(self.generate_GCode)
        d.addCallback(self._load_gcode)
        d.addCallback(self._start_executing)
        return d, on_success

    def _load_gcode(self, g_code):
        return self.controller.send_msg({"command": "put", "name": "program_upload", '0': 'gcode.cnc', '1': g_code})

    def _start_executing(self, _):
        return self.controller.send_msg({"command": "put", "name": "auto", "0": "AUTO_RUN", "1": 0})

    @abstractmethod
    def generate_GCode(self, _):
        pass
