# -*- coding: utf-8 -*-

from ConfigRequster import ConfigRequster
from MachineStatusWaiter import MachineStatusWaiter
from GCode import GCode


class MachineReseter(GCode):
    def __init__(self, controller, settings, yr_pin, red_led_pin, green_led_pin):
        GCode.__init__(self, controller)
        self.config_getter = ConfigRequster(controller, settings)
        self.machine_waiter = MachineStatusWaiter(controller)
        self.yr_pin = yr_pin
        self.red_led_pin = red_led_pin
        self.green_led_pin = green_led_pin

    def run(self, on_success):
        return self.reset_machine(None), on_success

    def generate_GCode(self, _):
        d = self.config_getter.get_config()
        d.addCallback(self.generate_reset_sequence)
        return d

    def reset_machine(self, _):
        d = self.send_abort(None)
        d.addCallback(self.set_auto_mode)
        d.addCallback(self.generate_GCode)
        d.addCallback(self._load_gcode)
        d.addCallback(self._start_executing)
        d.addCallback(self.machine_waiter.wait_machine_stop)
        return d

    def send_abort(self, _):
        return self.controller.send_msg({"command": "put", "name": "abort"})

    def generate_reset_sequence(self, _):
        # перейти на 0 по оси X\\
        # отключить YR
        # отключить подачу СОЖ
        # отключить шпиндель
        # Отключить светофор
        return """%
M65 P{}
M65 P{}
M65 P{}
G0 X0
M9
M5
M30
%""".format(self.yr_pin, self.red_led_pin, self.green_led_pin)
