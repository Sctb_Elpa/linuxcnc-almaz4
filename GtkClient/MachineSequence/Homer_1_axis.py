# -*- coding: utf-8 -*-

from twisted.internet import task

from IMachineSequence import IMachineSequence
from MachineStatusWaiter import MachineStatusWaiter


class Homer_1_axis(IMachineSequence):
    def __init__(self, controller):
        self.controller = controller
        self.stop_waiter = MachineStatusWaiter(controller)

    def run(self, arg):
        _axis, on_ready = arg
        return self.home_axis(_axis), on_ready

    def home_axis(self, _axis='X'):
        def send_home_cmd(_):
            axis2n = {'X': 0, 'Y': 1, 'Z': 2, 'A': 3, 'B': 4}
            if type(_axis) == str:
                axis = axis2n[_axis]
            else:
                axis = _axis
            print("sending home cmd axis {}".format(axis))
            self.controller.send_msg_no_reply({"command": "put", "name": "home", "0": str(axis)})

        def sleep_(s):
            return lambda _=None: task.deferLater(self.controller.get_reactor(), s, lambda _: None, None)

        d = self.controller.change_mode('MODE_MANUAL')
        d.addCallback(self.stop_waiter.wait_machine_stop)
        d.addCallback(send_home_cmd)
        d.addCallback(sleep_(1))
        d.addCallback(self.stop_waiter.wait_machine_stop)
        return d
