# -*- coding: utf-8 -*-

from IMachineSequence import IMachineSequence
from twisted.internet import defer


class Aborter(IMachineSequence):
    def __init__(self, controller):
        self.controller = controller

    def run(self, then_aborted):
        self.controller.abort(then_aborted)
        return defer.succeed(None), None
