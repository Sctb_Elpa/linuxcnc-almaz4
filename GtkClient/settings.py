# -*- coding: utf-8 -*-

import json
import math
import os
import sys


class ValueChangeRule:
    def __init__(self, _min=0.0, _max=1.0, step=1.0, is_loop=False):
        self.min = _min
        self.max = _max
        self.step = step
        self.is_loop = is_loop

    def change(self, val, increment):
        if increment is None:
            new_val = val + self.step
            if new_val > self.max:
                new_val = self.min if self.is_loop else self.max
        else:
            new_val = self.update(val + increment)
        return new_val

    def update(self, new_val):
        if new_val > self.max or new_val < self.min:
            if self.is_loop:
                if self.min == 0 and self.max == 1:
                    new_val = 1 if new_val == 0 else 0
                else:
                    ring = self.max / self.min
                    drop = math.floor(new_val / ring)
                    new_val = ring * (new_val - drop)
                    new_val = new_val + self.min if new_val > 0 else self.max + new_val
            else:
                new_val = self.min if new_val < self.min else self.max
        return new_val


class Settings:
    default_value_rules = {
        'Work Mode': ValueChangeRule(0, 2, 1, True),
        'G1 speed': ValueChangeRule(0.01, 10, 0.01, False),
        'Cutting depth': ValueChangeRule(0, 100.0, 1, False),
        'YR turns pre plate': ValueChangeRule(0, 500, 1, False),
        'Plate count': ValueChangeRule(1, 100, 1, False),
        'Lube enabled': ValueChangeRule(0, 1, 1, True),
        'Spindel Enabled': ValueChangeRule(0, 1, 1, True),
    }

    def __init__(self, file_name=''):
        self.file = file_name if file_name else os.path.join(os.path.dirname(sys.argv[0]), "settings.json")
        self.settings = {}
        self.value_rules = Settings.default_value_rules.copy()
        self._read_settings()

    def recheck_values(self):
        for key, val in self.settings.iteritems():
            rule = self.value_rules[key]
            new_val = rule.update(val)
            if new_val != val:
                print "Value {} was out from new limits, trimmed to {}".format(key, new_val)
                self.settings[key] = new_val

    # https://stackoverflow.com/questions/865115/how-do-i-correctly-clean-up-a-python-object
    # to use with
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.save_settings()

    def _read_settings(self):
        try:
            with open(self.file, 'r') as f:
                self.settings = json.load(f)
        except:
            # default settings
            for key in self.value_rules:
                self.settings[key] = (self.value_rules[key].min + self.value_rules[key].max) / 2

    def save_settings(self):
        s = ""
        try:
            with open(self.file, 'r') as pf:
                s = json.load(pf)
        except IOError:
            pass

        if cmp(s, self.settings) != 0:
            with open(self.file, 'w') as f:
                # красивый JSON
                json.dump(self.settings, f, sort_keys=True, indent=4, separators=(',', ': '))

    def __getitem__(self, key):
        if key in self.settings:
            return self.settings[key]
        else:
            raise KeyError(key)

    def __setitem__(self, key, value):
        if key in self.settings:
            rule = self.value_rules[key]
            old_val = self.settings[key]
            self.settings[key] = rule.update(value)
            print ('Settings[{}]: {} -> {}'.format(key, old_val, self.settings[key]))
        else:
            raise KeyError()

    def change_value(self, key, increment):
        if key in self.settings:
            rule = self.value_rules[key]
            old_val = self.settings[key]
            self.settings[key] = rule.change(old_val, increment)
            print ('Settings[{}]: {} -> {}'.format(key, old_val, self.settings[key]))

    def set_axis_min_limit(self, **kwargs):
        for key in Settings.default_value_rules:
            for axis in kwargs:
                if axis in key:
                    self.value_rules[key].min = kwargs[axis]

        self.recheck_values()

    def set_axis_max_limit(self, **kwargs):
        for key in Settings.default_value_rules:
            for axis in kwargs:
                if axis in key:
                    self.value_rules[key].max = kwargs[axis]

        self.recheck_values()
