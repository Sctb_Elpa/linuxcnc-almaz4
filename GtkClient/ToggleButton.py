# -*- coding: utf-8 -*-

import gobject
import gtk


class ToggleButton(gobject.GObject):
    __gproperties__ = {
        'toggle_on_click': (gobject.TYPE_BOOLEAN,
                            'toggle on click property',
                            'the toggle_on_click of the ToggleButton',
                            False,
                            gobject.PARAM_READWRITE
                            ),
        'repeat_on_holding': (gobject.TYPE_BOOLEAN,
                              'Calling _on_click() while holding the button',
                              'the Repeat_on_holding of the ToggleButton',
                              False,
                              gobject.PARAM_READWRITE
                              ),
        'blocked': (gobject.TYPE_BOOLEAN,
                    'Temporally disable button callback calls',
                    'the blocked of tne ToggleButton',
                    False,
                    gobject.PARAM_READWRITE
                    ),
    }

    def __init__(self, gtk_toggle_button):
        gobject.GObject.__init__(self)
        self.gtk_button = gtk_toggle_button
        self.gtk_button.connect('pressed', self.changed, True)
        self.gtk_button.connect('released', self.changed, False)
        self._toggle_on_click = False
        self._on_click = None
        self._cookie = None
        self._hold_timer_id = None
        self._repeat_timer_id = None
        self._accelerate_timer = None
        self._repeat_on_holding = False
        self._working_cookie = None
        self._blocked = False

        self.markup_template = u'<b>{}</b>'

    def changed(self, sender, val):
        if self._toggle_on_click:
            if val:
                self.gtk_button.set_active(self.gtk_button.get_active() == gtk.STATE_ACTIVE)
        else:
            self.gtk_button.set_active(val)

        if not val:
            self._stop_holding()
            if self._repeat_on_holding:
                self._update_label(self._cookie)

        if val and self._on_click:
            self._working_cookie = self._cookie
            self._start_holding()
            if not self._blocked:
                self._on_click(self._working_cookie)

    def _reset_repeat(self):
        if self._hold_timer_id:
            gobject.source_remove(self._hold_timer_id)
            self._hold_timer_id = None
        if self._repeat_timer_id:
            gobject.source_remove(self._repeat_timer_id)
            self._repeat_timer_id = None
        if self._accelerate_timer:
            gobject.source_remove(self._accelerate_timer)
            self._accelerate_timer = None

    def _start_holding(self):
        self._reset_repeat()
        if self.get_repeat_on_holding():
            self._hold_timer_id = gobject.timeout_add(1000, self._repeat_callback, False)

    def _stop_holding(self):
        self._reset_repeat()

    def _update_label(self, value):
        if value < 0:
            self.set_label(u'- {}'.format(-value))
        else:
            self.set_label(u'+ {}'.format(value))

    def _accelerate_spinning(self):
        self._working_cookie *= 10
        self._update_label(self._working_cookie)
        return True

    def _repeat_callback(self, userdata):
        if not userdata:
            self._repeat_timer_id = gobject.timeout_add(100, self._repeat_callback, True)
            self._accelerate_timer = gobject.timeout_add(1500, self._accelerate_spinning)
        if not self._blocked:
            self._on_click(self._working_cookie)
        return userdata

    def force_pushed_state(self, is_pushed):
        self.gtk_button.set_state(is_pushed)

    def set_label(self, text):
        label = self.gtk_button.get_child()
        label.set_markup(self.markup_template.format(text))
        label.props.wrap = True
        label.props.width_chars = 18
        label.set_justify(gtk.JUSTIFY_CENTER)

    def set_visible(self, is_visible):
        self.gtk_button.set_visible(is_visible)

    def set_sensitive(self, is_sensitive):
        self.gtk_button.set_sensitive(is_sensitive)

    def set_on_click(self, value, cookie):
        self._on_click = value
        self._cookie = cookie

    def get_on_click(self):
        return self._on_click, self._cookie

    # ---------------------------------------------------------------------

    def do_set_property(self, pspec, value):
        if pspec.name == 'toggle_on_click':
            self.set_toggle_on_click(value)
        elif pspec.name == 'repeat_on_holding':
            self.set_repeat_on_holding(value)
        elif pspec.name == 'blocked':
            self.set_blocked(value)
        else:
            raise AttributeError('unknown property %s' % pspec.name)

    def do_get_property(self, pspec):
        if pspec.name == 'toggle_on_click':
            return self.get_toggle_on_click()
        elif pspec.name == 'repeat_on_holding':
            return self.get_repeat_on_holding()
        elif pspec.name == 'blocked':
            return self.get_blocked()
        else:
            raise AttributeError('unknown property %s' % pspec.name)

    def set_toggle_on_click(self, value):
        self._toggle_on_click = value
        self.notify("toggle_on_click")

    def get_toggle_on_click(self):
        return self._toggle_on_click

    def set_repeat_on_holding(self, value):
        self._repeat_on_holding = value
        if not value:
            self._reset_repeat()
        self.notify("repeat_on_holding")

    def get_repeat_on_holding(self):
        return self._repeat_on_holding

    def set_blocked(self, is_blocked):
        self._blocked = is_blocked
        self.notify('blocked')

    def get_blocked(self):
        return self._blocked


# signals
# https://stackoverflow.com/questions/2057921/python-gtk-create-custom-signals
gobject.type_register(ToggleButton)
