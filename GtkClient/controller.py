# -*- coding: utf-8 -*-

from MachineSequence import *


class Controller:
    modes = {
        'MODE_MANUAL': 1,
        'MODE_AUTO': 2,
        'MODE_MDI': 3,
    }

    def __init__(self, factory, settings, diomap):
        self.factory = factory

        self.current_mode = Controller.modes['MODE_AUTO']
        self.executing_defered = None

        self.commands = {
            'Startup': StartupSequence(self),
            'Prepare': InitMachineSequence(self, settings, diomap['enable_ctl'], diomap['led_red'],
                                           diomap['yr_trigger'], diomap['yr_trigger'], diomap['YR']),
            'DisableMachine': DisableMachineSequence(self, settings, diomap['yr_trigger'], diomap['led_red'], diomap['yr_trigger']),
            'SystemShutdown': SystemShutdown(self, lambda: settings.save_settings()),
            'Reset': MachineReseter(self, settings, diomap['yr_trigger'], diomap['led_red'], diomap['yr_trigger']),
            'manual_goto': MDI_GO_Sequence(self),
            'manual_output_control': MDI_D_Out_Sequence(self),
            'Home_axis': Homer_1_axis(self),
            'Prepare_work_seq': PrepareSequence(self, settings, diomap['led_red'], diomap['led_red'],
                                                diomap['yr_trigger'], diomap['YR'], diomap['enable_ctl'], 10),
            'manual_MDI_nonblock': MDI_nonblock(self),
            'manual_MDI': MDIWait(self),
            'trigger_dy': YRTrigger(self, diomap['yr_trigger'], diomap['YR']),
            'abort': Aborter(self)
        }

    def process_command(self, _, command, arg):
        print "Command {}({})".format(command, arg)

        d, on_success = self.commands[command].run(arg)
        if on_success:
            d.addCallback(on_success)

        d.addCallbacks(self.finish, self.error)

        self.executing_defered = d

    def abort(self, then_aborted):
        if self.executing_defered:
            if then_aborted:
                self.executing_defered.addCallback(then_aborted)

            print('Controller.abort()')
            self.executing_defered.cancel()
            self.executing_defered = None
        else:
            if then_aborted:
                then_aborted()

    def error(self, err):
        print 'Controller.error({})'.format(err)
        return self.finish()

    def finish(self, _=None):
        self.executing_defered = None
        return True

    def reconnect(self, *args):
        self.factory.reconnect()

    def send_msg(self, msg):
        return self.factory.send_msg(msg)

    def send_msg_no_reply(self, msg):
        return self.factory.send_msg_no_reply(msg)

    def get_reactor(self):
        return self.factory.reactor

    def add_watch_list(self, variable):
        return self.factory.add_watch_list(variable)

    def change_mode(self, mode):
        mode_n = Controller.modes[mode]

        def mode_changed(msg):
            if msg[u'code'] == u'?OK':
                self.current_mode = mode_n
            else:
                raise RuntimeError(msg)

        def change_mode():
            print "Changing mode to {}".format(mode)
            _d = self.factory.send_msg({'command': 'put', 'name': 'mode', '0': mode})
            _d.addCallback(mode_changed)
            return _d

        def check_mode(real_mode):
            if mode != real_mode:
                return change_mode()

        d = self.get_current_mode()
        d.addCallback(check_mode)
        return d

    def get_current_mode(self, _=None):
        def _process_mode_ansver(msg):
            self.current_mode = msg[u'data']
            return self.current_mode

        d = self.factory.send_msg({'command': 'get', 'name': 'task_mode'})
        d.addCallback(_process_mode_ansver)
        return d
